﻿using Dashboard.Data.Domain.RestClient;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                var client = new RestClient("https://fulcrumware.atlassian.net");

                var res = await client.PostAsync("rest/auth/1/session", new
                {
                    username = "Matthew",
                    password = string.Empty
                });

                var x = 0;
            }).Wait();
        }
    }
}
