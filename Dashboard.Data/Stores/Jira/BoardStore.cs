﻿using Dashboard.Data.Contracts.Jira;
using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Domain.Queryables.Jira;
using Dashboard.Data.Entities.Jira;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Stores.Jira
{
    public class BoardStore : IJiraStore<Board>
    {
        private IRestClient _client;
        public BoardStore(IRestClient client)
        {
            _client = client;
        }

        public IQueryable<Board> Entities => new BoardQuery(_client);
    }
}
