﻿using Dashboard.Data.Contracts;
using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Entities.Jira;
using Dashboard.Data.Domain.Queryables.Jira;
using System.Linq;
using RestSharp;

namespace Dashboard.Data.Stores.Jira
{
    public class ProjectStore : IReadStore<Project>
    {
        private IRestClient _client;
        public ProjectStore(IRestClient client)
        {
            _client = client;
        }

        public IQueryable<Project> Entities => new JiraQuery<Project>($"{JiraApi.Project.Basic}?{JiraApi.Project.Expansion}", _client);
    }
}
