﻿using Dashboard.Data.Contracts;
using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Entities.Jira;
using Dashboard.Data.Domain.Queryables.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Dashboard.Data.Stores.Jira
{
    public class UserStore : IReadStore<User>
    {
        private IRestClient _client;
        public UserStore(IRestClient client)
        {
            _client = client;
        }

        public IQueryable<User> Entities => new JiraQuery<User>(JiraApi.User.All, _client);
    }
}
