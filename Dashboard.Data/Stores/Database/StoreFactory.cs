﻿using Dashboard.Data.Stores.Database;
using System.Data.Entity.Infrastructure;
using System;

namespace Dashboard.Data.Stores.Database
{
    public class StoreFactory : IDbContextFactory<BaseStore>
    {
        public BaseStore Create() => new BaseStore(null);
    }
}
