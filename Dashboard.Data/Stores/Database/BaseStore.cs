﻿using Dashboard.Data.Contracts.Database;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using System.Threading.Tasks;
using static System.Data.Entity.Database;

namespace Dashboard.Data.Stores.Database
{
    public class BaseStore : DbContext
    {
        private static MethodInfo StoreInitializer { get; } =
            typeof(System.Data.Entity.Database).GetMethod(nameof(SetInitializer));

        public BaseStore(IDbUser user) : base("TeamDashboard")
        {
            StoreInitializer.MakeGenericMethod(GetType()).Invoke(null, new object[] { null });
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.AddFromAssembly(typeof(DependencyConfiguration).Assembly);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public new async Task SaveChangesAsync()
        {
            await base.SaveChangesAsync();
        }
    }
}
