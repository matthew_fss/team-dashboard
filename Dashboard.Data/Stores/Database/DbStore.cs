﻿using Dashboard.Data.Contracts;
using Dashboard.Data.Contracts.Database;
using Dashboard.Data.Domain.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Stores.Database
{
    [DbConfigurationType(typeof(EntityFrameworkConfiguration))]
    public class DbStore<TEntity> : BaseStore, IStore<TEntity>
        where TEntity : class
    {
        public DbStore(IDbUser user) : base(user) { }

        private IDbSet<TEntity> EntitySet => Set<TEntity>();

        public IQueryable<TEntity> Entities => EntitySet;

        public TEntity Add(TEntity entity) => EntitySet.Add(entity);
    }
}
