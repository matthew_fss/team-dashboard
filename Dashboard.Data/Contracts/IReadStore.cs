﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Contracts
{
    public interface IReadStore<TEntity>
    {
        IQueryable<TEntity> Entities { get; }
    }
}
