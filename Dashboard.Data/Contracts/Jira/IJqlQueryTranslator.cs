﻿using Dashboard.Data.Domain.Jql;
using Dashboard.Data.Entities.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Contracts.Jira
{
    public interface IJqlQueryTranslator
    {
        JqlQuery Translate(Expression exp);
    }
}
