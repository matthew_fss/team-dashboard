﻿using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Entities.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Contracts.Jira
{
    public interface IJiraAuthenticationService
    {
        Task<JiraAuthenticationResponse> Authenticate(string username, string password);

        Task<bool> ValidateSession(string username, JiraSession session);
    }
}
