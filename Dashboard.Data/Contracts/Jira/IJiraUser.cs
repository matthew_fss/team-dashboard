﻿using Dashboard.Data.Domain.Jira;

namespace Dashboard.Data.Contracts.Jira
{
    public interface IJiraUser
    {
        string UserName { get; }
        JiraSession? Session { get; }
    }
}
