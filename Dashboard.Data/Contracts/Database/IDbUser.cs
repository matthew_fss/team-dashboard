﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Contracts.Database
{
    public interface IDbUser
    {
        Guid UserID { get; }

        string UserName { get; }
    }
}
