﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Contracts
{
    public interface IAuthenticationService
    {
        bool Authenticate(string username, string password);

        Task<bool> AuthenticateAsync(string username, string password);
    }
}
