﻿namespace Dashboard.Data.Contracts.Excel
{
    public interface IExcelUser
    {
        string UserName { get; }

        string ExcelCode { get; }
    }
}
