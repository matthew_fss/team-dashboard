﻿using Dashboard.Data.Entities.Excel;

namespace Dashboard.Data.Contracts.Excel
{
    public interface ITimeSheetFactory
    {
        ITimeSheetFactory ForYear(int year);

        ITimeSheetFactory ForMonth(int month);

        TimeSheet Get();
    }
}
