﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Contracts
{
    public interface IStore<TEntity> : IReadStore<TEntity>
    {
        TEntity Add(TEntity entity);

        Task SaveChangesAsync();
    }
}
