﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dashboard.Infrastructure.DependencyInjection;
using Dashboard.Data.Contracts;
using Dashboard.Data.Stores.Jira;
using Dashboard.Data.Entities.Jira;
using Dashboard.Data.Domain.Jira;
using System.Reflection;
using Dashboard.Data.Entities.Database;
using Dashboard.Data.Stores.Database;
using RestSharp;

namespace Dashboard.Data
{
    public class DependencyConfiguration : Infrastructure.DependencyInjection.DependencyConfiguration
    {
        public static IEnumerable<Type> DbEntities =>
            from t in typeof(DependencyConfiguration).Assembly.GetTypes()
            where t.IsClass && t.Namespace == typeof(Entities.Database.User).Namespace
            select t;

        public override void Configure(IDependencyInjector injector)
        {
            injector.TransientFactory<IRestClient>(di => new RestClient(JiraApi.Host))
                .TransientType<IReadStore<Entities.Jira.Project>, ProjectStore>();
                
            Assembly assembly = typeof(DependencyConfiguration).Assembly;
            //Registering DB Stores
            foreach(Type type in assembly.GetTypes().Where(t => t.IsClass && t.Namespace == typeof(Entities.Database.User).Namespace))
                injector.TransientType(typeof(IStore<>).MakeGenericType(type), typeof(DbStore<>).MakeGenericType(type));
        }
    }
}
