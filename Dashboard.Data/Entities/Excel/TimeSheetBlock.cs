﻿using System;

namespace Dashboard.Data.Entities.Excel
{
    public class TimeSheetBlock
    {
        public TimeSpan TimeOfDay { get; set; }

        public string CustomerCode { get; set; }

        public string ProjectCode { get; set; }

        public string JiraTicket { get; set; }

        public string Description { get; set; }

        public bool Valid { get; set; }
    }
}
