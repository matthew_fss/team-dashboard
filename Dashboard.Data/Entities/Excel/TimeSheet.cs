﻿using System.Collections.Generic;

namespace Dashboard.Data.Entities.Excel
{
    public class TimeSheet
    {
        public int Year { get; set; }

        public int Month { get; set; }

        public IEnumerable<TimeSheetDay> Days { get; set; }       
    }
}
