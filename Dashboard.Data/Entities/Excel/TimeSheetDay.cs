﻿using System;
using System.Collections.Generic;

namespace Dashboard.Data.Entities.Excel
{
    public class TimeSheetDay
    {
        public DateTime Day { get; set; }

        public IEnumerable<TimeSheetBlock> Blocks { get; set; }
    }
}
