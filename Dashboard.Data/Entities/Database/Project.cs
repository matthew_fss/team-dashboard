﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Entities.Database
{
    public class Project
    {
        public Guid ID { get; set; }
        public int JiraID { get; set; }
        public string Name { get; set; }
        public string Lead { get; set; }

        public Guid? BoardID { get; set; }

        public Board Board { get; set; }
    }
}
