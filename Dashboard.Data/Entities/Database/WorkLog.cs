﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Entities.Database
{
    public class WorkLog
    {
        public Guid IssueID { get; set; }
        public Guid UserID { get; set; }
        public DateTime LogDate { get; set; }
        public TimeSpan TimeLogged { get; set; }

        public Issue Issue { get; set; }
        public User User { get; set; }
    }
}
