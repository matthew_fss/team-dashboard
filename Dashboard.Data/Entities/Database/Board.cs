﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Entities.Database
{
    public class Board
    {
        public Guid ID { get; set; }

        public int JiraID { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}
