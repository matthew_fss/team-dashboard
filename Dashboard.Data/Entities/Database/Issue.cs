﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Entities.Database
{
    public class Issue
    {
        public Guid ID { get; set; }
        
        public DateTime RecordDate { get; set; }

        public int JiraID { get; set; }

        public string IssueType { get; set; }

        public Guid? SprintID { get; set; }
        
        public Sprint Sprint { get; set; }
        
        public string Epic { get; set; }

        public string Assignee { get; set; }

        public DateTime? Updated { get; set; }

        public string Status { get; set; }

        public string Summary { get; set; }

        public string Description { get; set; }

        public long? OriginalEstimate { get; set; }

        public long? RemainingEstimate { get; set; }

        public Guid BoardID { get; set; }

        public Board Board { get; set; }

        public virtual ICollection<WorkLog> WorkLogs { get; set; }
    }
}
