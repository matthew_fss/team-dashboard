﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Entities.Jira
{
    public class Project : BaseEntity
    {
        public string Description { get; set; }
        public User Lead { get; set; }
        public IDictionary<string, string> AvatarUrls { get; set; }
    }
}
