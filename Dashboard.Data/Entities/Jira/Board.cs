﻿using Dashboard.Data.Domain.Jql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Entities.Jira
{
    public class Board : BaseEntity
    {
        public string Type { get; set; }

        public virtual IQueryable<Project> Projects { get; set; }

        public virtual IQueryable<JiraApiIssue> Issues { get; set; }
    }
}
