﻿using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Domain.Jql;
using Dashboard.Infrastructure.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Entities.Jira
{
    public class Issue
    {
        [JqlField("Key")]
        public JiraKey Key { get; set; }

        [JqlField("Project")]
        public string Project { get; set; }

        [JqlField("Assignee")]
        public string Assignee { get; set; }

        [JqlField("Type")]
        public string Type { get; set; }

        [JqlField("Status")]
        public string Status { get; set; }

        [JqlField("Created", Formatter = typeof(DateTimeFormatter))]
        public DateTime? Created { get; set; }

        [JqlField("Summary")]
        public string Summary { get; set; }

        [JqlField("Description")]
        public string Description { get; set; }

        [JqlField("Epic Link")]
        public string Epic { get; set; }

        [JqlField("Reporter")]
        public string Reporter { get; set; }

        [JqlField("OriginalEstimate")]
        public StringToken OriginalEstimate { get; set; }

        [JqlField("OriginalEstimate", Formatter = typeof(EstimateFieldFormatter))]
        public int OriginalEstimateInSeconds { get; set; }

        [JqlField("RemainingEstimate")]
        public StringToken RemainingEstimate { get; set; }

        [JqlField("RemainingEstimate", Formatter = typeof(EstimateFieldFormatter))]
        public int RemainingEstimateInSeconds { get; set; }
    }
}
