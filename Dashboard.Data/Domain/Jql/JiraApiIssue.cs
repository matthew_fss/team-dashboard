﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jql
{
    public class JiraApiIssue
    {
        public string id;
        public string key;
        public JiraApiIssueField fields;
    }

    public class JiraApiIssueField
    {
        public JiraApiIssueType issuetype;
        public JiraApiProject project;
        public JiraResolution resolution;
        public DateTime created;
        public JiraApiIssueLink[] issuelinks;
        public JiraApiPerson assignee;
        public DateTime? updated;
        public JiraApiIssueStatus status;
        public string description;
        public string summary;
        public JiraApiPerson creator;
        public JiraApiPerson reporter;
        public JiraApiTimeTracking timetracking;
        public JiraEpic epic;
        [JsonExtensionData]
        public IDictionary<string, object> extra;
    }

    public class JiraApiIssueType
    {
        public string id;
        public string description;
        public string name;
    }

    public class JiraApiProject
    {
        public string id;
        public string key;
        public string name;
    }

    public class JiraApiIssueLink
    {
        public string id;
        public JiraApiIssueLinkType type;
        public JiraApiIssue inwardIssue;
    }

    public class JiraApiIssueLinkType
    {
        public string id;
        public string name;
        public string inward;
        public string outward;
    }

    public class JiraApiIssueStatus
    {
        public string description;
        public string name;
        public string id;
    }

    public class JiraApiPerson
    {
        public string name;
        public string key;
        public string emailAddress;
        public string displayName;
        public bool active;
        public string timeZone;
    }

    public class JiraApiTimeTracking
    {
        public string originalEstimate;
        public string remainingEstimate;
        public string timeSpent;
        public int originalEstimateSeconds;
        public int remainingEstimateSeconds;
        public int timeSpentSeconds;
    }

    public class JiraApiField
    {
        public string id;
        public string key;
        public string name;
        public bool custom;
        public bool orderable;
        public bool navigable;
        public bool searchable;
    }

    public class JiraResolution
    {
        public string id;
        public string description;
        public string name;
    }

    public class JiraEpic
    {
        public int id;
        public string key;
        public string name;
        public string summary;
    }
}
