﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Dashboard.Infrastructure.Tokens;
using System.Reflection;
using Dashboard.Data.Domain.Queryables;
using Dashboard.Infrastructure.Expressions;
using Dashboard.Data.Contracts.Jira;

namespace Dashboard.Data.Domain.Jql
{
    public class JqlQueryTranslator : Infrastructure.Expressions.ExpressionVisitor, IJqlQueryTranslator
    {
        private StringBuilder _where;
        private StringBuilder _orderby;

        private bool whereScope = false;
        private bool firstWhere = true;
        private bool orderScope = false;

        private JqlField? fields = null;

        public JqlQuery Translate(Expression exp)
        {
            exp = new LocalExpressionEvaluator(test => (test as MethodCallExpression)?.Method.Name == "Include").Evaluate(exp);

            _where = new StringBuilder();
            _orderby = new StringBuilder();

            Visit(exp);

            if (_where.Length != 0 && _orderby.Length != 0)
                _where.Append(" ");

            _where.Append(_orderby);

            return new JqlQuery()
            {
                Query = _where.ToString(),
                Fields = fields ?? JqlField.Fulcrum
            };
        }

        protected override Expression VisitMethodCall(MethodCallExpression m)
        {
            Expression next = null;
            if (m.Arguments[0].Type.IsAssignableFrom(typeof(IssueQuery)) && m.Method.Name == "Where")
            {
                next = ScopedWhere(m);
            }
            else if (m.Arguments[0].Type.IsAssignableFrom(typeof(IssueQuery)) &&
                (m.Method.Name == "OrderBy"
                || m.Method.Name == "OrderByDescending"
                || m.Method.Name == "ThenBy"
                || m.Method.Name == "ThenByDescending"))
            {
                next = ScopedOrderBy(m);
            }
            else if (whereScope || orderScope)
                next = ScopedSpecialMethod(m, negate: false);
            else if (m.Method.Name == "Include")
                next = ProcessInclude(m);

            if(next != null)
                return base.Visit(next);

            return m;
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
            if(whereScope)
            {
                if (_where.Length != 0 && !firstWhere)
                    _where.Append(" AND ");

                switch(b.NodeType)
                {
                    case ExpressionType.Equal:
                        ProcessEqualityOperator(b, negate: false);
                        break;
                    case ExpressionType.NotEqual:
                        ProcessEqualityOperator(b, negate: true);
                        break;
                    case ExpressionType.GreaterThan:
                        ProcessCompareOperator(b, Operator.GreaterThan);
                        break;
                    case ExpressionType.GreaterThanOrEqual:
                        ProcessCompareOperator(b, Operator.GreaterThanOrEqualTo);
                        break;
                    case ExpressionType.LessThan:
                        ProcessCompareOperator(b, Operator.LessThan);
                        break;
                    case ExpressionType.LessThanOrEqual:
                        ProcessCompareOperator(b, Operator.LessThanOrEqualTo);
                        break;
                    case ExpressionType.AndAlso:
                        ProcessLogicalOperator(b, Operator.And);
                        break;
                    case ExpressionType.OrElse:
                        ProcessLogicalOperator(b, Operator.Or);
                        break;
                    default:
                        throw new NotSupportedException($"Operator '{b.NodeType}' is not supported");
                }

                return b;
            }

            return base.VisitBinary(b);
        }

        protected override Expression VisitUnary(UnaryExpression u)
        {
            if(whereScope)
            {
                if (_where.Length != 0 && !firstWhere)
                    _where.Append(" AND ");

                switch(u.NodeType)
                {
                    case ExpressionType.Not:
                        if (u.Operand is MethodCallExpression)
                            ScopedSpecialMethod(u.Operand as MethodCallExpression, negate: true);
                        else
                            ProcessPrefixOperator(u, Operator.Not);
                        break;
                }

                return u;
            }

            return base.VisitUnary(u);
        }

        //protected override Expression VisitConstant(ConstantExpression c)
        //{
        //    if (c.Value is IssueQuery)
        //        Visit((c.Value as IQueryable<Issue>).Expression);

        //    return base.VisitConstant(c);
        //}

        private Expression ScopedWhere(MethodCallExpression m)
        {
            Expression next = m.Arguments[0];
            LambdaExpression lambda = ((LambdaExpression)((UnaryExpression)m.Arguments[1]).Operand);
            Expression exp;
            whereScope = true;
            
            exp = VisitLambda(lambda);

            firstWhere = false;
            whereScope = false;

            return next;
        }

        private Expression ScopedOrderBy(MethodCallExpression m)
        {
            throw new NotImplementedException();
        }

        private Expression ScopedSpecialMethod(MethodCallExpression m, bool negate)
        {
            if (m.Method.Name == "Contains")
                ProcessContains(m, negate);

            return null;
        }

        private void ProcessEqualityOperator(BinaryExpression b, bool negate)
        {
            JqlFieldAttribute field = GetExpressionField(b.Left);
            object value = GetExpressionValue(b);
            Operator op;

            if (value == null && negate)
                op = Operator.IsNot;
            else if (value == null && !negate)
                op = Operator.Is;
            else if (value != null && negate)
                op = Operator.NotEqual;
            else
                op = Operator.Equal;

            value = value == null ? "empty" : value;

            FinishCompareProcess(field.FieldName, field.Format(value), op);
        }

        private void ProcessCompareOperator(BinaryExpression b, Operator op)
        {
            JqlFieldAttribute field = GetExpressionField(b.Left);

            FinishCompareProcess(field.FieldName, field.Format(GetExpressionValue(b)), op);
        }

        private void FinishCompareProcess(string field, string value, Operator op)
            => _where.Append($"{QuerySafeString(field)} {op} {QuerySafeString(value)}");
        
        private void ProcessLogicalOperator(BinaryExpression b, Operator op)
        {
            _where.Append("(");
            Visit(b.Left);
            _where.Append($" {op} ");
            Visit(b.Right);
            _where.Append(")");
        }

        private void ProcessPrefixOperator(UnaryExpression u, Operator op)
        {
            _where.Append($"{op} (");
            Visit(u.Operand);
            _where.Append($")");
        }

        private void ProcessContains(MethodCallExpression m, bool negate)
        {
            if (m.Object?.NodeType == ExpressionType.MemberAccess && m.Object?.Type == typeof(string))
                ProcessStringContains(m, negate);
            else
                ProcessCollectionContains(m, negate);
        }

        private void ProcessCollectionContains(MethodCallExpression m, bool negate)
        {
            var isarray = m.Object == null;
            var objList = GetExpressionValue(isarray ? m.Arguments[0] : m.Object);

            if (!(objList is IEnumerable<object>))
                throw new NotSupportedException($"The Contains method is not supported for type '{objList.GetType()}'");

            JqlFieldAttribute field = GetExpressionField(m.Arguments[isarray ? 1 : 0]);

            var op = negate ? Operator.NotIn : Operator.In;
            _where.Append($"{QuerySafeString(field.FieldName)} {op} ({string.Join(", ", (objList as IEnumerable<object>).Select(x => QuerySafeString(field.Format(x))))})");
        }

        private void ProcessStringContains(MethodCallExpression exp, bool negate)
        {
            var field = GetExpressionField(exp.Object);
            var obj = GetExpressionValue(exp.Arguments[0]);
            var op = negate ? Operator.NotContains : Operator.Contains;

            _where.Append($"{QuerySafeString(field.FieldName)} {op} {QuerySafeString(obj?.ToString())}");
        }

        private Expression ProcessInclude(MethodCallExpression exp)
        {
            var include = (JqlField)GetExpressionValue(exp.Arguments[1]);
            if (fields.HasValue)
                fields |= include;
            else
                fields = include;

            return exp.Arguments[0];
        }

        private JqlFieldAttribute GetExpressionField(Expression exp)
        {
            PropertyInfo property = null;
            JqlFieldAttribute field = null;
            if (TryGetProperty(exp, out property) && (field = property.GetCustomAttribute<JqlFieldAttribute>()) != null)
            {
                return field;
            }
            
            if(property == null)
                throw new NotSupportedException($"Unknown Jql Field");

            throw new NotSupportedException($"The field {property.Name} is not supported");
        }

        private object GetExpressionValue(Expression exp)
        {
            if (exp is BinaryExpression)
                return GetExpressionValue(((BinaryExpression)exp).Right);
            else if (exp is UnaryExpression)
                return GetExpressionValue(((UnaryExpression)exp).Operand);
            else if (exp is ConstantExpression)
                return ((ConstantExpression)exp).Value;
            else if (exp is MethodCallExpression)
            {
                var method = (MethodCallExpression)exp;
                var info = method.Method;

                object obj = method.Object == null
                    ? null
                    : GetExpressionValue(method.Object);

                object[] array = new object[method.Arguments.Count];

                for (int i = 0; i < array.Length; i++)
                    array[i] = GetExpressionValue(method.Arguments[i]);

                return info.Invoke(obj, array);
            }
            else if (exp is NewArrayExpression)
            {
                object[] items = new object[((NewArrayExpression)exp).Expressions.Count];
                for (int i = 0; i < ((NewArrayExpression)exp).Expressions.Count; i++)
                    items[i] = (GetExpressionValue(((NewArrayExpression)exp).Expressions[i]));

                return items;
            }
            else if (exp is MemberInitExpression)
            {
                var memberInit = exp as MemberInitExpression;
                object[] items = new object[memberInit.NewExpression.Arguments.Count];
                for (int i = 0; i < memberInit.NewExpression.Arguments.Count; i++)
                    items[i] = (GetExpressionValue(memberInit.NewExpression.Arguments[i]));

                return memberInit.NewExpression.Constructor.Invoke(items);
            }
            else if (exp is MemberExpression)
            {
                var member = exp as MemberExpression;
                var parentValue = GetExpressionValue(member.Expression);

                if (member.Member is FieldInfo)
                    return ((FieldInfo)member.Member).GetValue(parentValue);
                else if (member.Member is PropertyInfo)
                    return ((PropertyInfo)member.Member).GetValue(parentValue);
            }
            else if(exp is ListInitExpression)
            {
                var listInit = exp as ListInitExpression;
                object[] args = new object[listInit.NewExpression.Arguments.Count];
                for (int i = 0; i < listInit.NewExpression.Arguments.Count; i++)
                    args[i] = GetExpressionValue(listInit.NewExpression.Arguments[i]);

                var new_list = listInit.NewExpression.Constructor.Invoke(args);
                
                for (int i = 0; i < listInit.Initializers.Count; i++)
                {
                    object[] initializer_args = new object[listInit.Initializers[i].Arguments.Count];
                    
                    for (int j = 0; j < listInit.Initializers[i].Arguments.Count; j++)
                        initializer_args[j] = GetExpressionValue(listInit.Initializers[i].Arguments[j]);

                    listInit.Initializers[i].AddMethod.Invoke(new_list, initializer_args);
                }

                return new_list;
            }

            throw new NotSupportedException($"The expression {exp} is not supported at this level.");
        }

        private bool TryGetProperty(Expression exp, out PropertyInfo property)
            => (property = (exp as MemberExpression)?.Member as PropertyInfo) != null;

        private string QuerySafeString(string value)
        {
            value = value ?? "\"\"";

            return value.Any(char.IsWhiteSpace) ? $"\"{value}\"" : value;
        }

        private class Operator : StringToken
        {
            private Operator(string value) : base(value) { }

            public static Operator Equal = new Operator("=");
            public static Operator Is = new Operator("is");
            public static Operator NotEqual = new Operator("!=");
            public static Operator IsNot = new Operator("is not");
            public static Operator GreaterThan = new Operator(">");
            public static Operator GreaterThanOrEqualTo = new Operator(">=");
            public static Operator LessThan = new Operator("<");
            public static Operator LessThanOrEqualTo = new Operator("<=");
            public static Operator Not = new Operator("NOT");
            public static Operator And = new Operator("AND");
            public static Operator Or = new Operator("OR");
            public static Operator Contains = new Operator("~");
            public static Operator NotContains = new Operator("!~");
            public static Operator In = new Operator("in");
            public static Operator NotIn = new Operator("not in");
        }
    }
}
