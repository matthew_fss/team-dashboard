﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jql
{
    [Flags]
    public enum JqlField
    {
        Summary = 1 << 0,
        Description = 1 << 1,
        TimeTracking = 1 << 2,
        IssueType = 1 << 3,
        Project = 1 << 4,
        Resolution = 1 << 5,
        Created = 1 << 6,
        Status = 1 << 7,
        Reporter = 1 << 8,
        Assignee = 1 << 9,
        Updated = 1 << 10,
        Attachments = 1 << 11,
        Comments = 1 << 12,
        IssueLinks = 1 << 13,
        Fulcrum = Summary
            | Description
            | TimeTracking
            | IssueType
            | Project
            | Resolution
            | Created
            | Status
            | Reporter
            | Assignee
            | Updated,

        [Description("*navigable")]
        Navigable = 1 << 14,

        [Description("*all")]
        All = ~0
    }
}
