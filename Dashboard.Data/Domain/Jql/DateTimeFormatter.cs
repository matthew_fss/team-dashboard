﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jql
{
    public class DateTimeFormatter : JqlFieldFormatter
    {
        public override string Format(object value)
        {
            DateTime? date = value as DateTime?;

            if (date == null)
                return base.Format(value);

            return date.Value.ToString("MM/dd/yyyy");
        }
    }
}
