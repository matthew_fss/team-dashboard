﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jql
{
    public class JqlFieldFormatter
    {
        public virtual string Format(object value)
            => value?.ToString();
    }
}
