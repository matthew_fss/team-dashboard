﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jql
{
    public class JqlQuery
    {
        public string Query { get; set; }

        public JqlField Fields { get; set; }
    }
}
