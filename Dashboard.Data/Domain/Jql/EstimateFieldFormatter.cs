﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jql
{
    public class EstimateFieldFormatter : JqlFieldFormatter
    {
        public override string Format(object value)
        {
            int? estimate = value as int?;
            if (estimate == null)
                return base.Format(value);

            return (estimate.Value / 60M).ToString();
        }
    }
}
