﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jql
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class JqlFieldAttribute : Attribute
    {
        public JqlFieldAttribute(string name)
        {
            FieldName = name;
        }

        public string FieldName { get; private set; }

        public Type Formatter { get; set; }
        
        public string Format(object value) => GetFormatter().Format(value);

        private JqlFieldFormatter formatter;
        private JqlFieldFormatter GetFormatter()
        {
            if (formatter != null)
                return formatter;

            if (Formatter != null && typeof(JqlFieldFormatter).IsAssignableFrom(Formatter))
                formatter = Activator.CreateInstance(Formatter) as JqlFieldFormatter;

            return formatter = (formatter ?? new JqlFieldFormatter());
        }
    }
}
