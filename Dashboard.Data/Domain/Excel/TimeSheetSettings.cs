﻿using Dashboard.Data.Contracts.Excel;
using Dashboard.Infrastructure.Clock;
using Dashboard.Infrastructure.FileSystem;
using System;
using System.Globalization;
using System.IO;

namespace Dashboard.Data.Domain.Excel
{
    public class TimeSheetSettings : ITimeSheetSettings
    {
        private IClock _clock;
        private IFileSystem _fileSystem;

        public TimeSheetSettings(IClock clock, IFileSystem fileSystem)
        {
            _clock = clock;
            _fileSystem = fileSystem;
        }

        public string TimeTrackingBasePath { get; } = @"\\fss\share\TimeTracking";

        public string CustomerColumnName { get; } = "Cust";

        public string JiraTicketColumnName { get; } = "JIRA Ticket";

        public string ProjectColumnName { get; } = "Project";

        public string DescriptionColumnName { get; } = "Description";

        public string TimeColumnName { get; } = "Time";

        public string TrackingEndCell { get; } = "I73";

        public string TrackingStartCell { get; } = "E1";

        public string TimeSheetFilePath(int year, int month, string excelCode)
        {
            if (year < 2009 || year > _clock.Now.Year)
                throw new ArgumentOutOfRangeException($"Year {year} is invalid.  The year must be later than 2008 and before or the same as the current year.");

            if (month < 1 || month > 12)
                throw new ArgumentOutOfRangeException($"Month {month} is invalid.  The month must be between 1 and 12 inclusively");

            if (year == _clock.Now.Year && month > _clock.Now.Month)
                throw new ArgumentOutOfRangeException($"Month {month} is invalid for year {year}.");

            string fileName = $"{GetMonthName(month)} {year} - {excelCode}.xlsx";
            string path = $@"{TimeTrackingBasePath}\{year}\{fileName}";
            
            if(!_fileSystem.FileExists(path))
            {
                string archive_path = $@"{TimeTrackingBasePath}\{year}\Archive\{fileName}";
                
                if (!_fileSystem.FileExists(archive_path))
                    throw new InvalidOperationException($"{fileName} not found.  Tried: {path} and {archive_path}");

                path = archive_path;
            }

            return path;
        }

        public string WorksheetName(int day)
        {
            if (day < 1 || day > 31)
                throw new ArgumentOutOfRangeException($"Day {day} is not valid.  A valid worksheet day is a number between 1 and 31");

            return $"{day}{GetDaySuffix(day)}";
        }

        public string WorksheetName(TimeSheetPage page)
        {
            switch(page)
            {
                case TimeSheetPage.Allocated:
                    return "A";
                case TimeSheetPage.Overview:
                    return "O";
                default:
                    return WorksheetName((int)page);
            }
        }

        private string GetDaySuffix(int day)
            => day % 10 == 1 && day != 11
                ? "st"
                : day % 10 == 2 && day != 12
                    ? "nd"
                    : day % 10 == 3 && day != 13
                        ? "rd"
                        : "th";

        private string GetMonthName(int month)
        {
            return new DateTime(2000, month, 1).ToString("MMMM", CultureInfo.InvariantCulture);
        }
    }
}
