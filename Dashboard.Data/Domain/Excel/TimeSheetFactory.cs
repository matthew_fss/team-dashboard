﻿using Dashboard.Data.Contracts.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dashboard.Data.Entities.Excel;
using Dashboard.Infrastructure.Clock;
using LinqToExcel;
using System.Globalization;

namespace Dashboard.Data.Domain.Excel
{
    public class TimeSheetFactory : ITimeSheetFactory
    {
        private IExcelUser _user;
        private IClock _clock;
        private ITimeSheetSettings _settings;

        public TimeSheetFactory(IExcelUser user, IClock clock, ITimeSheetSettings settings)
        {
            _user = user;
            _clock = clock;
            _settings = settings;
        }

        private int? _month;
        public ITimeSheetFactory ForMonth(int month)
        {
            _month = month;
            return this;
        }

        private int? _year;
        public ITimeSheetFactory ForYear(int year)
        {
            _year = year;
            return this;
        }

        public TimeSheet Get()
        {
            int year = _year ?? _clock.Now.Year;
            int month = _month ?? _clock.Now.Month;

            return new TimeSheet()
            {
                Month = month,
                Year = year,
                Days = GetTimeSheetDays(year, month)
            };
        }

        private IEnumerable<TimeSheetDay> GetTimeSheetDays(int year, int month)
        {
            using (var factory = new ExcelQueryFactory(_settings.TimeSheetFilePath(year, month, _user.ExcelCode))
            {
                ReadOnly = true
            })
            {
                factory.AddMapping<TimeSheetBlock>(x => x.CustomerCode, _settings.CustomerColumnName);
                factory.AddMapping<TimeSheetBlock>(x => x.ProjectCode, _settings.ProjectColumnName);
                factory.AddMapping<TimeSheetBlock>(x => x.Description, _settings.DescriptionColumnName);
                factory.AddMapping<TimeSheetBlock>(x => x.JiraTicket, _settings.JiraTicketColumnName);
                factory.AddMapping<TimeSheetBlock>(x => x.TimeOfDay, _settings.TimeColumnName);

                factory.AddTransformation<TimeSheetBlock>(x => x.TimeOfDay, time =>
                {
                    return TimeSpan.Parse(time);
                });

                return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                    .Select(i => new TimeSheetDay()
                    {
                        Day = new DateTime(year, month, i),
                        Blocks = factory.WorksheetRange<TimeSheetBlock>(_settings.TrackingStartCell, _settings.TrackingEndCell, _settings.WorksheetName(i))
                    });
            }
        }
    }
}
