﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.RestClient
{
    public static class RestClientExtensions
    {
        public static async Task<IRestResponse> GetAsync(this IRestClient client, string url)
            => await client.ExecuteTaskAsync(CreateRequest(Method.GET, url));

        public static async Task<IRestResponse<T>> GetAsync<T>(this IRestClient client, string url)
            => await client.ExecuteTaskAsync<T>(CreateRequest(Method.GET, url));

        public static async Task<T> GetAsAsync<T>(this IRestClient client, string url)
            => GetResult(await GetAsync<T>(client, url));

        public static async Task<IRestResponse> PostAsync(this IRestClient client, string url, object data)
            => await client.ExecuteTaskAsync(CreateRequest(Method.POST, url, data));

        public static async Task<IRestResponse<T>> PostAsync<T>(this IRestClient client, string url, object data)
            => await client.ExecuteTaskAsync<T>(CreateRequest(Method.POST, url, data));

        public static async Task<T> PostAsAsync<T>(this IRestClient client, string url, object data)
            => GetResult(await PostAsync<T>(client, url, data));

        public static async Task<IRestResponse> PutAsync(this IRestClient client, string url, object data)
            => await client.ExecuteTaskAsync(CreateRequest(Method.PUT, url, data));

        public static async Task<IRestResponse<T>> PutAsync<T>(this IRestClient client, string url, object data)
            => await client.ExecuteTaskAsync<T>(CreateRequest(Method.PUT, url, data));

        public static async Task<T> PutAsAsync<T>(this IRestClient client, string url, object data)
            => GetResult(await PutAsync<T>(client, url, data));

        public static async Task<IRestResponse> DeleteAsync(this IRestClient client, string url)
            => await client.ExecuteTaskAsync(CreateRequest(Method.DELETE, url));

        private static RestRequest CreateRequest(Method method, string resource, object data = null)
        {
            var request = new RestRequest()
            {
                Method = method,
                Resource = resource,
                RequestFormat = DataFormat.Json
            };

            if (data != null)
                request.AddJsonBody(data);

            return request;
        }

        private static T GetResult<T>(IRestResponse<T> response)
        {
            if(response.StatusCode == System.Net.HttpStatusCode.Forbidden ||
                response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                throw new AuthenticationException($"{response.StatusCode}: {response.StatusDescription}");

            if ((int)response.StatusCode < 200 || (int)response.StatusCode >= 300)
                throw new InvalidOperationException($"{response.StatusCode}: {response.ErrorMessage}");

            return response.Data;
        }
    }
}
