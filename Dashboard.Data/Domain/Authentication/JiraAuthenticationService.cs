﻿using Dashboard.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dashboard.Data.Entities.Jira;
using RestSharp;
using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Contracts.Jira;
using Dashboard.Data.Domain.RestClient;

namespace Dashboard.Data.Domain.Authentication
{
    public class JiraAuthenticationService : IAuthenticationService, IJiraAuthenticationService
    {
        private IRestClient _client;
        public JiraAuthenticationService(IRestClient client)
        {
            _client = client;
        }

        bool IAuthenticationService.Authenticate(string username, string password)
        {
            return ((IAuthenticationService)this).AuthenticateAsync(username, password).Result;
        }

        public async Task<JiraAuthenticationResponse> Authenticate(string username, string password)
        {
            var response = new JiraAuthenticationResponse();

            var res = await _client.PostAsync(JiraApi.Authentication.Session, new
            {
                username = username,
                password = password
            }).ConfigureAwait(false);

            RestResponseCookie cookie;
            if (res.StatusCode != System.Net.HttpStatusCode.OK || (cookie = res.Cookies.FirstOrDefault(x => x.Name == JiraApi.CrowdCookieKey)) == null)
                return JiraAuthenticationResponse.Unauthorized();

            return JiraAuthenticationResponse.Authorized(cookie.Value);
        }

        async Task<bool> IAuthenticationService.AuthenticateAsync(string username, string password)
        {
            JiraAuthenticationResponse response = await Authenticate(username, password);

            return response.Authenticated;
        }

        public async Task<bool> ValidateSession(string username, JiraSession session)
        {
            IRestRequest request = new RestRequest()
            {
                Method = Method.GET,
                Resource = JiraApi.Authentication.Session,
                RequestFormat = DataFormat.Json,
            }.AddCookie(JiraApi.CrowdCookieKey, session.Token);

            var res = await _client.ExecuteTaskAsync(request);

            return res.StatusCode == System.Net.HttpStatusCode.OK;
        }
    }
}
