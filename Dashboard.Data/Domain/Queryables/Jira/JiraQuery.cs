﻿using Dashboard.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dashboard.Data.Entities.Jira;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.Threading;
using System.Collections;
using Dashboard.Infrastructure.Queries;
using Dashboard.Data.Contracts.Jira;
using RestSharp;
using Dashboard.Data.Domain.RestClient;

namespace Dashboard.Data.Domain.Queryables.Jira
{
    public abstract class JiraQuery<TEntity, TJiraResponse> : IQueryable<TEntity>, IQueryProvider, IDbAsyncQueryProvider
    {
        protected IRestClient _client;
        protected string _resource;

        private AsyncEnumerableQuery<TEntity> query;

        public JiraQuery(string resource, IRestClient client)
        {
            _resource = resource;
            _client = client;

            query = new AsyncEnumerableQuery<TEntity>(() => GetAsync().Result);
        }

        protected abstract TEntity Mapping(TJiraResponse response);

        public virtual async Task<IEnumerable<TEntity>> GetAsync()
            => (await _client.GetAsAsync<IEnumerable<TJiraResponse>>(_resource)).Select(Mapping);

        #region IQueryable
        Type IQueryable.ElementType { get; } = typeof(TEntity);

        Expression IQueryable.Expression => ((IQueryable)query).Expression;

        #endregion

        #region IQueryProvider
        IQueryable IQueryProvider.CreateQuery(Expression expression)
            => query.CreateQuery(expression);

        IQueryable<TElement> IQueryProvider.CreateQuery<TElement>(Expression expression)
            => query.CreateQuery<TElement>(expression);

        IQueryProvider IQueryable.Provider => query.Provider;

        object IQueryProvider.Execute(Expression expression)
        {
            return ((IDbAsyncQueryProvider)this).ExecuteAsync(expression, default(CancellationToken)).Result;
        }

        TResult IQueryProvider.Execute<TResult>(Expression expression)
        {
            return ((IDbAsyncQueryProvider)this).ExecuteAsync<TResult>(expression, default(CancellationToken)).Result;
        }
        #endregion

        #region IDbAsyncQueryProvider
        async Task<object> IDbAsyncQueryProvider.ExecuteAsync(Expression expression, CancellationToken cancellationToken)
            => await query.ExecuteAsync(expression, cancellationToken);

        async Task<TResult> IDbAsyncQueryProvider.ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
            => await query.ExecuteAsync<TResult>(expression, cancellationToken);
        #endregion

        #region IEnumerable
        IEnumerator<TEntity> IEnumerable<TEntity>.GetEnumerator()
            => ((IEnumerable<TEntity>)query).GetEnumerator();

        public IEnumerator GetEnumerator()
            => ((IEnumerable<TEntity>)query).GetEnumerator();

        #endregion
    }

    public class JiraQuery<TEntity> : JiraQuery<TEntity, TEntity>
    {
        public JiraQuery(string resource, IRestClient client) : base(resource, client) { }

        protected override TEntity Mapping(TEntity response) => response;
    }
}
