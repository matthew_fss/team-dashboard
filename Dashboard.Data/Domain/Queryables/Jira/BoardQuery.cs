﻿using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Entities.Jira;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Queryables.Jira
{
    public class BoardQuery : PagedJiraQuery<Board>
    {
        public BoardQuery(IRestClient client) : base(JiraApi.Board.Basic, client)
        {
        }

        protected override Board Mapping(Board response)
        {
            response.Projects = new PagedJiraQuery<Project>($"{JiraApi.Board.Basic}/{response.Id}/project", _client);
            response.Issues = new PagedIssueQuery($"{JiraApi.Board.Basic}/{response.Id}/issue", _client);

            return response;
        }
    }
}
