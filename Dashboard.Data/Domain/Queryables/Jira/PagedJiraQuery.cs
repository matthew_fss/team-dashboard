﻿using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Domain.Jql;
using Dashboard.Data.Domain.RestClient;
using Dashboard.Data.Entities.Jira;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Queryables.Jira
{
    public abstract class BasePagedJiraQuery<TPageResult, TEntity> : JiraQuery<TEntity>
        where TPageResult : PagedResult
    {
        public BasePagedJiraQuery(string resource, IRestClient client) : base(resource, client) { }

        protected abstract IEnumerable<TEntity> GetPageEntities(TPageResult page);

        public virtual Task<TPageResult> GetPageAsync(int start = 0, int? take = null)
            => _client.GetAsAsync<TPageResult>($"{_resource}?startAt={start}&maxResults={take}");

        public override async Task<IEnumerable<TEntity>> GetAsync()
        {
            IEnumerable<TEntity> collection = new TEntity[0];

            TPageResult page = null;
            int index = 0;
            int total = 0;
            do
            {
                page = await GetPageAsync(start: index);

                var items = GetPageEntities(page).Select(Mapping);
                total = items.Count();
                if (total != 0)
                    collection = collection.Union(items);

                index += total;
            } while (total != 0 && !page.IsLast);

            return collection;
        }
    }

    public class PagedJiraQuery<TEntity> : BasePagedJiraQuery<PagedResult<TEntity>, TEntity>
    {
        public PagedJiraQuery(string resource, IRestClient client) : base(resource, client) { }

        protected override IEnumerable<TEntity> GetPageEntities(PagedResult<TEntity> page)
            => page.Values;
    }

    public class PagedIssueQuery : BasePagedJiraQuery<PagedIssueResult, JiraApiIssue>
    {
        public PagedIssueQuery(string resource, IRestClient client) : base(resource, client) { }

        protected override IEnumerable<JiraApiIssue> GetPageEntities(PagedIssueResult page)
            => page.Issues;
    }
}
