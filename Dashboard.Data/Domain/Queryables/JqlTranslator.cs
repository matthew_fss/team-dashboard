﻿using Dashboard.Infrastructure.Querying;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Dashboard.Infrastructure.Tokens;
using System.Reflection;

namespace Dashboard.Data.Queryables
{
    public class JqlTranslator : Infrastructure.Querying.ExpressionVisitor
    {
        private StringBuilder _where;
        private StringBuilder _orderby;

        private bool whereScope = false;
        private bool firstWhere = true;
        private bool orderScope = false;
        private bool methodScope = false;

        public string Translate(Expression exp)
        {
            _where = new StringBuilder();
            _orderby = new StringBuilder();

            Visit(exp);

            if (_where.Length != 0 && _orderby.Length != 0)
                _where.Append(" ");

            _where.Append(_orderby);

            return _where.ToString();
        }

        protected override Expression VisitMethodCall(MethodCallExpression m)
        {
            if (typeof(IssueQuery).IsAssignableFrom(m.Arguments[0].Type) && m.Method.Name == "Where")
            {
                return ScopedWhere(m);
            }
            else if (m.Method.Name == "OrderBy"
                || m.Method.Name == "OrderByDescending"
                || m.Method.Name == "ThenBy"
                || m.Method.Name == "ThenByDescending")
            {
                return ScopedOrderBy(m);
            }
            else if(whereScope || orderScope)
                return ScopedSpecialMethod(m);

            throw new NotSupportedException($"Query '{m.Method}' is not supported");
        }

        private Expression ScopedWhere(MethodCallExpression m)
        {
            LambdaExpression lambda = ((LambdaExpression)((UnaryExpression)m.Arguments[1]).Operand);
            Expression exp;
            whereScope = true;

            exp = VisitLambda(lambda);

            firstWhere = false;
            whereScope = false;

            return exp;
        }

        private Expression ScopedOrderBy(MethodCallExpression m)
        {
            return null;
        }

        private Expression ScopedSpecialMethod(MethodCallExpression m)
        {
            return base.VisitMethodCall(m);
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
            if(whereScope)
            {
                if (_where.Length != 0 && !firstWhere)
                    _where.Append(" AND ");

                switch(b.NodeType)
                {
                    case ExpressionType.Equal:
                        ProcessCompareOperator(b, Operator.Equal);
                        break;
                    case ExpressionType.NotEqual:
                        ProcessCompareOperator(b, Operator.NotEqual);
                        break;
                    case ExpressionType.AndAlso:
                        ProcessLogicalOperator(b, Operator.And);
                        break;
                    case ExpressionType.OrElse:
                        ProcessLogicalOperator(b, Operator.Or);
                        break;
                    default:
                        throw new NotSupportedException($"Operator '{b.NodeType}' is not supported");
                }
            }

            return b;
        }

        protected override Expression VisitUnary(UnaryExpression u)
        {
            if(whereScope)
            {
                if (_where.Length != 0 && !firstWhere)
                    _where.Append(" AND ");

                switch(u.NodeType)
                {
                    case ExpressionType.Not:
                        ProcessPrefixOperator(u, Operator.Not);
                        break;
                }
            }

            return u;
        }

        private void ProcessCompareOperator(BinaryExpression b, Operator op)
        {
            string property = GetBinaryExpressionPropertyName(b);
            object value = GetBinaryExpressionValue(b);
            string value_text = value == null && (op == Operator.Equal || op == Operator.NotEqual)
                ? "empty"
                : (value?.ToString() ?? "\"\"");
            string op_text;
            if (value == null && op == Operator.Equal)
                op_text = "is";
            else if (value == null && op == Operator.NotEqual)
                op_text = "is not";
            else
                op_text = op;

            value_text = value_text.Any(char.IsWhiteSpace) ? $"\"{value_text}\"" : value_text;

            _where.Append($"{property} {op_text} {value_text}");
        }

        private void ProcessLogicalOperator(BinaryExpression b, Operator op)
        {
            _where.Append("(");
            Visit(b.Left);
            _where.Append($" {op} ");
            Visit(b.Right);
            _where.Append(")");
        }

        private void ProcessPrefixOperator(UnaryExpression u, Operator op)
        {
            _where.Append($"{op} (");
            Visit(u.Operand);
            _where.Append($")");
        }

        private string GetBinaryExpressionPropertyName(BinaryExpression b)
        {
            PropertyInfo property = null;
            if (TryGetProperty(b, out property))
                return property.Name;                

            throw new NotSupportedException("Binary operators can only be applied to object properties or indexes");
        }

        private object GetBinaryExpressionValue(BinaryExpression b)
        {
            Expression valueExpression = b.Left is MemberExpression ? b.Right : b.Left;

            if (valueExpression.NodeType == ExpressionType.Constant)
            {
                return ((ConstantExpression)valueExpression).Value;
            }

            throw new NotSupportedException("Binary operators can only be applied to constant values");
        }

        private bool TryGetProperty(BinaryExpression b, out PropertyInfo property)
            => (property = (b.Left as MemberExpression ?? b.Right as MemberExpression)?.Member as PropertyInfo) != null;

        private class Operator : StringToken
        {
            private Operator(string value) : base(value) { }

            public static Operator Equal = new Operator("=");
            public static Operator NotEqual = new Operator("!=");
            public static Operator Not = new Operator("NOT");
            public static Operator And = new Operator("AND");
            public static Operator Or = new Operator("OR");
        }
    }
}
