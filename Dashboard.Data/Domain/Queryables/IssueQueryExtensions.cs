﻿using Dashboard.Data.Domain.Jql;
using Dashboard.Data.Entities.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Queryables
{
    public static class IssueQueryExtensions
    {
        private static MethodInfo GetInfo<T1, T2, T3>(Func<T1, T2, T3> func, T1 one, T2 two)
        {
            return func.Method;
        }

        public static IQueryable<Issue> Include(this IQueryable<Issue> query, JqlField fields)
        {
            if (query == null)
                throw new ArgumentNullException(nameof(query));

            Expression<Func<IQueryable<Issue>, JqlField>> exp = x => fields;

            return query.Provider.CreateQuery<Issue>(
                Expression.Call(
                    null,
                    GetInfo(Include, query, fields),
                    new Expression[] { query.Expression, Expression.Constant(fields) }
                )
            );
        }
    }
}
