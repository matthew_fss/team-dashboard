﻿using Dashboard.Data.Entities.Jira;
using Dashboard.Infrastructure.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Queryables
{
    public class IssueQuery : Queryable<Issue>
    {
        public IssueQuery(IssueQueryProvider provider) : base(provider) { }

        public IssueQuery(IssueQueryProvider provider, Expression exp) : base(provider, exp) { }
    }
}
