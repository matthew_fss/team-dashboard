﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Dashboard.Data.Entities.Jira;
using Dashboard.Infrastructure.Tokens;
using Dashboard.Data.Domain.Jql;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Dashboard.Infrastructure.Expressions;
using System.Data.Entity.Infrastructure;
using System.Threading;
using Dashboard.Infrastructure.Queries;
using RestSharp;
using Dashboard.Data.Domain.RestClient;

namespace Dashboard.Data.Domain.Queryables
{
    public class IssueQueryProvider : QueryProvider, IDbAsyncQueryProvider
    {
        private JqlQueryTranslator translator = new JqlQueryTranslator();

        private Expression _issueQueryExpression;

        private IRestClient _client;
        public IssueQueryProvider(IRestClient client)
        {
            _client = client;
        }

        public IssueQueryProvider(IRestClient client, Expression exp)
        {
            _client = client;
            _issueQueryExpression = exp;
        }

        public override IQueryable<TEntity> CreateQuery<TEntity>(Expression expression)
        {
            if (typeof(TEntity) == typeof(Issue))
                return (new IssueQuery(this, expression)) as IQueryable<TEntity>;
            
            var next_query = new EnumerableQuery<Issue, Issue>(() => 
                _issueQueryExpression == null
                ? new Issue[0]
                : ((IQueryProvider)this).Execute<IEnumerable<Issue>>(_issueQueryExpression).AsEnumerable()
            );

            return new EnumerableQuery<TEntity, Issue>(new ReplaceIssueQueryExpressionRewriter(next_query.Expression).Visit(expression));
        }

        public override object Execute(Expression expression)
        {
            return ExecuteAsync(expression, default(CancellationToken)).Result;
        }

        public override string GetQueryText(Expression expression) 
            => translator.Translate(expression).Query;

        private static IDictionary<string, JiraApiField> customFields;
        private static object GetCustomField(IRestClient client, JiraApiIssueField field, string name)
        {
            if (customFields == null)
                customFields = client.GetAsAsync<IEnumerable<JiraApiField>>("/rest/api/2/field")
                    .Result
                    .Where(x => x.custom == true)
                    .ToDictionary(x => x.name, x => x);

            var field_id = customFields[name].id;
            return field.extra.ContainsKey(field_id)
                ? field.extra[field_id]
                : null;
        }

        public async Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken)
        {
            var jql = translator.Translate(expression);
            var result = (await _client.PostAsAsync<IEnumerable<JiraApiIssue>>("rest/api/2/search", new
            {
                jql = jql.Query,
                startAt = 0,
                maxResults = 50,
                fields = new string[] { "*navigable", "timetracking" }
            }).ConfigureAwait(false)
            ).Select(remoteIssue =>
                {
                    return new Issue()
                    {
                        Assignee = remoteIssue?.fields.assignee?.displayName,
                        Created = remoteIssue.fields.created,
                        Description = remoteIssue?.fields.description,
                        Key = remoteIssue.key,
                        OriginalEstimate = remoteIssue.fields.timetracking.originalEstimate,
                        OriginalEstimateInSeconds = remoteIssue.fields.timetracking.originalEstimateSeconds,
                        Project = remoteIssue.fields.project.key,
                        RemainingEstimate = remoteIssue.fields.timetracking.remainingEstimate,
                        RemainingEstimateInSeconds = remoteIssue.fields.timetracking.remainingEstimateSeconds,
                        Reporter = remoteIssue.fields.reporter.name,
                        Status = remoteIssue.fields.status.name,
                        Summary = remoteIssue.fields.summary,
                        Type = remoteIssue.fields.issuetype.name,
                        //Epic = GetCustomField(_client, remoteIssue.fields, CustomField.EpicLink)?.ToString()
                    };
                });

            return result;
        }

        public async Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            return (TResult)await ExecuteAsync(expression, cancellationToken);
        }

        private class ReplaceIssueQueryExpressionRewriter : Infrastructure.Expressions.ExpressionVisitor
        {
            private Expression _replacement;
            public ReplaceIssueQueryExpressionRewriter(Expression replacement)
            {
                _replacement = replacement;
            }

            protected override Expression VisitMethodCall(MethodCallExpression m)
            {
                if (m.Arguments.Count > 0 && m.Arguments[0] is ConstantExpression && ((ConstantExpression)m.Arguments[0]).Value is IssueQuery)
                    return _replacement;

                return base.VisitMethodCall(m);
            }
        }
    }
}
