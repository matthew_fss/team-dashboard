﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Queryables
{
    public class EnumerableQuery<TEntity, TBase> : IQueryable<TEntity>, IQueryProvider, IDbAsyncQueryProvider
    {
        private Expression _expression;

        public EnumerableQuery(IEnumerable<TBase> collection) : this(() => collection) { }

        public EnumerableQuery(Func<IEnumerable<TBase>> factory)
        {
            _expression = Expression.Constant(
                new EnumerableQuery<TBase>(
                    Expression.Call(
                        Expression.Constant(factory.Target), 
                        factory.Method
                    )
                )
            );
        }

        internal EnumerableQuery(Expression expression)
        {
            _expression = expression;
        } 

        public Type ElementType => typeof(TEntity);

        public Expression Expression => _expression;

        public IQueryProvider Provider => this;

        public IQueryable CreateQuery(Expression expression)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            if (expression == null)
                throw new ArgumentNullException(nameof(expression));
            if (!typeof(IQueryable<TElement>).IsAssignableFrom(expression.Type))
                throw new ArgumentException(nameof(expression));

            return new EnumerableQuery<TElement, TBase>(expression);
        }

        public object Execute(Expression expression) => ExecuteAsync(expression, default(CancellationToken)).Result;

        public TResult Execute<TResult>(Expression expression) => ExecuteAsync<TResult>(expression, default(CancellationToken)).Result;

        public Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => ((IQueryable)new EnumerableQuery<TBase>(Enumerable.Empty<TBase>())).Provider.Execute<TResult>(expression));
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return Execute<IEnumerable<TEntity>>(_expression).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
