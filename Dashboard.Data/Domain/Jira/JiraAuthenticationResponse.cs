﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jira
{
    public class JiraAuthenticationResponse
    {
        public bool Authenticated { get; set; }

        public JiraSession Session { get; set; }

        public static JiraAuthenticationResponse Unauthorized()
        {
            return new JiraAuthenticationResponse() { Authenticated = false };
        }

        public static JiraAuthenticationResponse Authorized(string session)
        {
            return new JiraAuthenticationResponse()
            {
                Authenticated = true,
                Session = new JiraSession()
                {
                    Token = session
                }
            };
        }
    }
}
