﻿using Dashboard.Infrastructure.Tokens;
using System.Collections.Generic;

namespace Dashboard.Data.Domain.Jira
{
    public class CustomField : StringToken
    {
        private static List<CustomField> instances = new List<CustomField>();
        private CustomField(string value) : base(value, true)
        {
            instances.Add(this);
        }

        public static CustomField EpicLink = new CustomField("Epic Link");
        public static CustomField EpicName = new CustomField("Epic Name");

        public static IEnumerable<CustomField> GetValues()
        {
            return instances;
        }
    }
}
