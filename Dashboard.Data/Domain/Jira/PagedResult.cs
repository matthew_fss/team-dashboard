﻿using Dashboard.Data.Domain.Jql;
using Dashboard.Data.Entities.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jira
{
    public class PagedResult
    {
        public int StartAt { get; set; }

        public int MaxResults { get; set; }

        public int Total { get; set; }

        public bool IsLast { get; set; }
    }

    public class PagedResult<T> : PagedResult
    {
        public IEnumerable<T> Values { get; set; }
    }

    public class PagedIssueResult : PagedResult
    {
        public IEnumerable<JiraApiIssue> Issues { get; set; }
    }
}
