﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jira
{
    public class JiraKey : IComparable<string>, IComparable<JiraKey>
    {
        private string _value;
        public JiraKey(string value)
        {
            _value = value;
        }

        public static bool operator ==(JiraKey left, JiraKey right)
            => StringComparer.OrdinalIgnoreCase.Compare(left._value, right._value) == 0;
        
        public static bool operator !=(JiraKey left, JiraKey right) => !(left == right);

        public static bool operator >(JiraKey left, JiraKey right)
            => StringComparer.OrdinalIgnoreCase.Compare(left._value, right._value) > 0;

        public static bool operator >=(JiraKey left, JiraKey right)
            => StringComparer.OrdinalIgnoreCase.Compare(left._value, right._value) >= 0;

        public static bool operator <(JiraKey left, JiraKey right) => !(left >= right);

        public static bool operator <=(JiraKey left, JiraKey right) => !(left > right);

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj is JiraKey)// || obj is string || obj is Atlassian.Jira.ComparableString)
                return (JiraKey)obj == this;
            else if (obj is string)
                return new JiraKey((string)obj) == this;

            return base.Equals(obj);
        }

        public override int GetHashCode() => _value.GetHashCode();

        public override string ToString() => _value;

        public int CompareTo(string other)
        {
            return CompareTo((JiraKey)other);
        }

        public int CompareTo(JiraKey other)
        {
            return StringComparer.OrdinalIgnoreCase.Compare(_value, other._value);
        }

        public static implicit operator JiraKey(string value) => new JiraKey(value);
    }
}
