﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jira
{
    public struct JiraSession
    {
        public string Token { get; set; }
    }
}
