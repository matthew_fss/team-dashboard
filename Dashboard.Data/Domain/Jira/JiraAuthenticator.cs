﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using RestSharp.Authenticators;
using RestSharp;
using Dashboard.Data.Entities.Jira;

namespace Dashboard.Data.Domain.Jira
{
    public class JiraAuthenticator : IAuthenticator
    {
        private static string[] Anonymous = new string[]
        {
            JiraApi.Authentication.Session
        };

        public JiraAuthenticator(JiraSession session)
        {
            Session = session;
        }

        public JiraSession Session { get; }

        public void Authenticate(IRestClient client, IRestRequest request)
        {
            if (Session.Token != null)
                request.AddCookie(JiraApi.CrowdCookieKey, Session.Token);
        }
    }
}
