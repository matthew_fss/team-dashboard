﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Domain.Jira
{
    public static class JiraApi
    {
        public const string Host = "https://fulcrumware.atlassian.net";
        public const string CrowdCookieKey = "studio.crowd.tokenkey";

        public static class Authentication
        {
            public const string Session = "rest/auth/1/session";
        }

        public static class Board
        {
            public const string Basic = "rest/agile/1.0/board";
        }

        public static class Project
        {
            public const string Basic = "rest/api/2/project";
            public const string Expansion = "expand=description,lead";
        }

        public static class User
        {
            public const string All = "rest/api/2/user/search?username=%";
        }
    }
}
