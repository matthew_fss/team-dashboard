﻿using Dashboard.Data.Entities.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Configurations.Database
{
    public class ProjectConfiguration : EntityTypeConfiguration<Project>
    {
        public ProjectConfiguration()
        {
            HasKey(x => x.ID);
            Property(x => x.Name).IsRequired().HasMaxLength(256);
            Property(x => x.Lead).IsOptional().HasMaxLength(256);
            Property(x => x.JiraID).IsRequired();

            HasOptional(x => x.Board).WithMany().HasForeignKey(x => x.BoardID);
        }
    }
}
