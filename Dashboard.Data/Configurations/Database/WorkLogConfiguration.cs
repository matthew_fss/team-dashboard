﻿using Dashboard.Data.Entities.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Configurations.Database
{
    public class WorkLogConfiguration : EntityTypeConfiguration<WorkLog>
    {
        public WorkLogConfiguration()
        {
            HasKey(x => new
            {
                x.IssueID,
                x.UserID,
                x.LogDate
            });

            Property(x => x.TimeLogged).IsRequired();

            HasRequired(x => x.Issue).WithMany().HasForeignKey(x => x.IssueID);
            HasRequired(x => x.User).WithMany().HasForeignKey(x => x.UserID);
        }
    }
}
