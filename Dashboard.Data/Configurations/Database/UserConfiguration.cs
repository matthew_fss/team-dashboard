﻿using Dashboard.Data.Entities.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Configurations.Database
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(x => x.ID);

            Property(x => x.UserName)
                .HasMaxLength(256)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_UserName", 1) { IsUnique = true }
                    )
                );

            Property(x => x.EmailAddress)
                .HasMaxLength(256)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_EmailAddress", 1) { IsUnique = true }
                    )
                );

            Property(x => x.ExcelCode)
                .HasMaxLength(5)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_ExcelCode", 5) { IsUnique = true }
                    )
                );
        }
    }
}
