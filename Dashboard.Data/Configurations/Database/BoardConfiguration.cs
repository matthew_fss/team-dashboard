﻿using Dashboard.Data.Entities.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Configurations.Database
{
    public class BoardConfiguration : EntityTypeConfiguration<Board>
    {
        public BoardConfiguration()
        {
            HasKey(x => x.ID);

            Property(x => x.JiraID).IsRequired();
        }
    }
}
