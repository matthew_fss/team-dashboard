﻿using Dashboard.Data.Entities.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Configurations.Database
{
    public class SprintConfiguration : EntityTypeConfiguration<Sprint>
    {
        public SprintConfiguration()
        {
            HasKey(x => x.ID);
            Property(x => x.State).HasMaxLength(10).IsRequired();
            Property(x => x.StartDate).IsOptional();
            Property(x => x.EndDate).IsOptional();
            Property(x => x.CompletedDate).IsOptional();

            HasRequired(x => x.Project).WithMany().HasForeignKey(x => x.ProjectID);
        }
    }
}
