﻿using Dashboard.Data.Entities.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.Configurations.Database
{
    public class IssueConfiguration : EntityTypeConfiguration<Issue>
    {
        public IssueConfiguration()
        {
            HasKey(x => x.ID);
            Property(x => x.IssueType).IsOptional();
            Property(x => x.Epic).IsOptional();
            Property(x => x.Assignee).IsOptional();
            Property(x => x.Updated).IsOptional();
            Property(x => x.Status).IsOptional();
            Property(x => x.Summary).IsOptional();
            Property(x => x.Description).IsOptional();
            Property(x => x.OriginalEstimate).IsOptional();
            Property(x => x.RemainingEstimate).IsOptional();

            HasOptional(x => x.Sprint).WithMany().HasForeignKey(x => x.SprintID);
            HasRequired(x => x.Board).WithMany().HasForeignKey(x => x.BoardID);
        }
    }
}
