﻿using Dashboard.Data.Contracts.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Helpers
{
    public class TestExcelUser : IExcelUser
    {
        public static TestExcelUser Instance = new TestExcelUser("TEST", "TEST USER");

        public TestExcelUser(string code, string name)
        {
            ExcelCode = code;
            UserName = name;
        }

        public string ExcelCode { get; }

        public string UserName { get; }
    }
}
