﻿using Dashboard.Infrastructure.Clock;
using System;

namespace Dashboard.Tests.Helpers
{
    public class MockClock : IClock
    {
        public DateTime Now { get; private set; }

        public DateTime UtcNow { get; private set; }

        public MockClock SetNow(DateTime now)
        {
            Now = now;
            return this;
        }

        public MockClock SetUtcNow(DateTime utcNow)
        {
            UtcNow = utcNow;
            return this;
        }
    }
}
