﻿using Dashboard.Infrastructure.FileSystem;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Helpers
{
    public class MockFileSystem : Mock<IFileSystem>, IFileSystem
    {
        private static string ProjectDirectory { get; } = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

        public string CombinePath(params string[] paths) => Path.Combine(paths);

        public bool FileExists(string path) => Object.FileExists(path);

        public byte[] ReadFile(string path) => Object.ReadFile(path);

        /// <summary>
        /// Sets up FileExists and ReadFile so they return true and 
        /// a byte array respectively for the param path using the
        /// param proxy as the actual file path.
        /// </summary>
        /// <param name="path">The file path that will be used during testing</param>
        /// <param name="proxy">A relative path from the root of the testing project</param>
        /// <returns>The same MockFileSystem object for chaining purposes.</returns>
        public MockFileSystem AddFileProxy(string path, string proxy)
            => AddFileProxy(path, () => File.ReadAllBytes(CombinePath(ProjectDirectory, proxy)));

        /// <summary>
        /// Sets up FileExists and ReadRile so they return true and
        /// a byte array respectively for the param path using the
        /// param file as data.
        /// </summary>
        /// <param name="path">The file path that will be used during testing</param>
        /// <param name="file">An array of bytes that represents the file</param>
        /// <returns>The same MockFileSystem object for chaining purposes.</returns>
        public MockFileSystem AddFileProxy(string path, byte[] file)
            => AddFileProxy(path, () => file);

        private MockFileSystem AddFileProxy(string path, Func<byte[]> retriever)
        {
            Setup(x => x.FileExists(path)).Returns(true);
            Setup(x => x.ReadFile(path)).Returns(retriever);

            return this;
        }
    }
}
