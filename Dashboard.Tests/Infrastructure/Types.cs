﻿using Dashboard.Infrastructure.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Infrastructure
{
    public interface INotImplementedInterface { }

    public interface ITestInterface1 { }

    public interface ITestInterface2 { }

    public abstract class NotInheritedAbstractClass { }

    public abstract class BaseTestAbstractClass { }

    public class NotInheritedTestClass { }

    public class BaseTestClass { }

    public class SingleInterfaceImplementationClass : ITestInterface1 { }

    public class AbstractClassImplementationClass : BaseTestAbstractClass { }

    public class SubTestClass : BaseTestClass { }

    public interface IGenericInterface1<T> { }

    public interface IGenericInterface2<T> { }

    public class SingleGenericInterfaceImplementationClass : IGenericInterface1<BaseTestClass> { }

    public class MultipleGenericImplementation : IGenericInterface1<BaseTestClass>, IGenericInterface1<bool>, IGenericInterface2<int>, ITestInterface2 { }

    public class TestQuery : IQuery<BaseTestClass> { }

    public class TestQueryHandler : IQueryHandler<TestQuery, BaseTestClass>
    {
        public BaseTestClass Execute(TestQuery query) { return null; }
    }

    public class TestAsyncQuery : IAsyncQuery<BaseTestClass> { }

    public class TestAsyncQueryHandler : IAsyncQueryHandler<TestAsyncQuery, BaseTestClass>
    {
        public Task<BaseTestClass> ExecuteAsync(TestAsyncQuery query)
        {
            return Task.FromResult(new BaseTestClass());
        }
    }
}
