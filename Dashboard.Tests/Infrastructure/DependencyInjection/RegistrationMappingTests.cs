﻿using Dashboard.Infrastructure.DependencyInjection;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Infrastructure.DependencyInjection
{
    [TestClass]
    public class RegistrationMappingTests
    {
        [TestClass]
        public class GenericOfTypeMethod
        {
            [TestMethod]
            public void For_A_Concrete_Type_With_No_Subclasses()
            {
                var types = Registration.Classes.OfType<NotInheritedTestClass>();
                types.Should().HaveCount(1);
                types.First().Should().Be(typeof(NotInheritedTestClass));
            }

            [TestMethod]
            public void For_A_Concrete_Type_That_Has_Subclasses()
            {
                var types = Registration.Classes.OfType<BaseTestClass>();
                types.Should().HaveCount(2);
                types.ShouldAllBeEquivalentTo(new[] { typeof(BaseTestClass), typeof(SubTestClass) });
            }

            [TestMethod]
            public void For_An_Abstract_Class_With_No_Subclasses()
            {
                var types = Registration.Classes.OfType<NotInheritedAbstractClass>();
                types.Should().HaveCount(0);
            }

            [TestMethod]
            public void For_An_Abstract_Class_That_Has_A_Subclass()
            {
                var types = Registration.Classes.OfType<BaseTestAbstractClass>();
                types.Should().HaveCount(1);
                types.ShouldAllBeEquivalentTo(new []{ typeof(AbstractClassImplementationClass) });
            }

            [TestMethod]
            public void For_An_Interface_With_No_Implementations()
            {
                var types = Registration.Classes.OfType<INotImplementedInterface>();
                types.Should().HaveCount(0);
            }

            [TestMethod]
            public void For_An_Interface_With_Implementations()
            {
                var types = Registration.Classes.OfType<ITestInterface2>();
                types.Should().HaveCount(1);
                types.ShouldAllBeEquivalentTo(new[] { typeof(MultipleGenericImplementation) });
            }

            [TestMethod]
            public void For_A_Generic_Interface_With_No_Implementations()
            {
                var types = Registration.Classes.OfType<IGenericInterface1<string>>();
                types.Should().HaveCount(0);
            }

            [TestMethod]
            public void For_A_Generic_Interface_With_Implementations()
            {
                var types = Registration.Classes.OfType<IGenericInterface1<BaseTestClass>>();
                types.Should().HaveCount(2);
                types.ShouldAllBeEquivalentTo(new[] { typeof(SingleGenericInterfaceImplementationClass), typeof(MultipleGenericImplementation) });
            }
        }

        [TestClass]
        public class OfTypeMethod
        {
            [TestMethod]
            public void For_A_Concrete_Type_With_No_Subclasses()
            {
                var types = Registration.Classes.OfType(typeof(NotInheritedTestClass));
                types.Should().HaveCount(1);
                types.First().Should().Be(typeof(NotInheritedTestClass));
            }

            [TestMethod]
            public void For_A_Concrete_Type_That_Has_Subclasses()
            {
                var types = Registration.Classes.OfType(typeof(BaseTestClass));
                types.Should().HaveCount(2);
                types.ShouldAllBeEquivalentTo(new[] { typeof(BaseTestClass), typeof(SubTestClass) });
            }

            [TestMethod]
            public void For_An_Abstract_Class_With_No_Subclasses()
            {
                var types = Registration.Classes.OfType(typeof(NotInheritedAbstractClass));
                types.Should().HaveCount(0);
            }

            [TestMethod]
            public void For_An_Abstract_Class_That_Has_A_Subclass()
            {
                var types = Registration.Classes.OfType(typeof(BaseTestAbstractClass));
                types.Should().HaveCount(1);
                types.ShouldAllBeEquivalentTo(new[] { typeof(AbstractClassImplementationClass) });
            }

            [TestMethod]
            public void For_An_Interface_With_No_Implementations()
            {
                var types = Registration.Classes.OfType(typeof(INotImplementedInterface));
                types.Should().HaveCount(0);
            }

            [TestMethod]
            public void For_An_Interface_With_Implementations()
            {
                var types = Registration.Classes.OfType(typeof(ITestInterface2));
                types.Should().HaveCount(1);
                types.ShouldAllBeEquivalentTo(new[] { typeof(MultipleGenericImplementation) });
            }

            [TestMethod]
            public void For_A_Generic_Interface_With_No_Implementations()
            {
                var types = Registration.Classes.OfType(typeof(IGenericInterface1<string>));
                types.Should().HaveCount(0);
            }

            [TestMethod]
            public void For_A_Generic_Interface_With_Implementations()
            {
                var types = Registration.Classes.OfType(typeof(IGenericInterface1<BaseTestClass>));
                types.Should().HaveCount(2);
                types.ShouldAllBeEquivalentTo(new[] { typeof(SingleGenericInterfaceImplementationClass), typeof(MultipleGenericImplementation) });
            }
            
            [TestMethod]
            public void For_An_Open_Generic_Interface()
            {
                var types = Registration.Classes.OfType(typeof(IGenericInterface1<>));
                types.Should().HaveCount(2);
                types.ShouldAllBeEquivalentTo(new[] { typeof(SingleGenericInterfaceImplementationClass), typeof(MultipleGenericImplementation) });
            }
        }
    }
}
