﻿using Dashboard.Infrastructure.DependencyInjection;
using FluentAssertions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Infrastructure.DependencyInjection
{
    [TestClass]
    public class UnityDependencyInjectorTest
    {
        [TestClass]
        public class Register_Type
        {
            [TestMethod]
            public void Can_Register_And_Resolve_Concrete_Type()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType(typeof(BaseTestClass));

                var instance = injector.Resolve<BaseTestClass>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<BaseTestClass>();
            }

            [TestMethod]
            public void Can_Register_But_Not_Resolve_Interface()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType(typeof(ITestInterface1));

                var instance = injector.Resolve<ITestInterface1>();
                instance.Should().BeNull();
            }

            [TestMethod]
            public void Can_Register_But_Not_Resolve_Abstract_Class()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType(typeof(BaseTestAbstractClass));

                var instance = injector.Resolve<BaseTestAbstractClass>();
                instance.Should().BeNull();
            }
        }

        [TestClass]
        public class Register_From_And_To_Type
        {
            [TestMethod]
            public void Register_Class_For_Interface()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType(typeof(ITestInterface1), typeof(SingleInterfaceImplementationClass));

                var instance = injector.Resolve<ITestInterface1>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<SingleInterfaceImplementationClass>();
            }

            [TestMethod]
            public void Generic_Register_Class_For_Interface()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType<ITestInterface1, SingleInterfaceImplementationClass>();

                var instance = injector.Resolve<ITestInterface1>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<SingleInterfaceImplementationClass>();
            }

            [TestMethod]
            public void Register_Class_For_Abstract_Class()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType(typeof(BaseTestAbstractClass), typeof(AbstractClassImplementationClass));

                var instance = injector.Resolve<BaseTestAbstractClass>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<AbstractClassImplementationClass>();
            }

            [TestMethod]
            public void Generic_Register_Class_For_Abstract_Class()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType<BaseTestAbstractClass, AbstractClassImplementationClass>();

                var instance = injector.Resolve<BaseTestAbstractClass>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<AbstractClassImplementationClass>();
            }

            [TestMethod]
            public void Register_Subclass_For_Class()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType(typeof(BaseTestClass), typeof(SubTestClass));

                var instance = injector.Resolve<BaseTestClass>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<SubTestClass>();
            }

            [TestMethod]
            public void Generic_Register_Subclass_For_Class()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterType<BaseTestClass, SubTestClass>();

                var instance = injector.Resolve<BaseTestClass>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<SubTestClass>();
            }
        }

        [TestClass]
        public class Register_Factory
        {
            [TestMethod]
            public void Can_Register_Interface_Using_Function()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterFactory(typeof(ITestInterface1), di => new SingleInterfaceImplementationClass());

                var instance = injector.Resolve<ITestInterface1>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<SingleInterfaceImplementationClass>();
            }

            [TestMethod]
            public void Can_Register_Abstract_Class_Using_Function()
            {
                var injector = new UnityDependencyInjector();
                injector.RegisterFactory(typeof(BaseTestAbstractClass), di => new AbstractClassImplementationClass());

                var instance = injector.Resolve<BaseTestAbstractClass>();
                instance.Should().NotBeNull();
                instance.Should().BeOfType<AbstractClassImplementationClass>();
            }
        }

        [TestClass]
        public class Register_Using_Registration
        {
            [TestMethod]
            public void Can_Register_Open_Generic()
            {
                var injector = new UnityDependencyInjector();
                injector.Register(Registration.ForOpenGeneric(typeof(IGenericInterface1<>)));

                var result = injector.Resolve<IGenericInterface1<bool>>();
                result.Should().NotBeNull();
                result.Should().BeOfType<MultipleGenericImplementation>();

                var two = injector.Resolve<IGenericInterface1<BaseTestClass>>();
                two.Should().BeOfType<MultipleGenericImplementation>();
            }
        }
    }
}
