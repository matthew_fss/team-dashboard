﻿using Dashboard.Infrastructure.Tokens;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Infrastructure
{
    [TestClass]
    public class StringTokenTest
    {
        protected void TestEqual(StringToken a, StringToken b)
        {
            a.Should().Be(b);
            (a == b).Should().BeTrue();
            (a != b).Should().BeFalse();
            a.GetHashCode().Should().Be(b.GetHashCode());
        }

        protected void TestNotEqual(StringToken a, StringToken b)
        {
            a.Should().NotBe(b);
            (a == b).Should().BeFalse();
            (a != b).Should().BeTrue();
            a.GetHashCode().Should().NotBe(b.GetHashCode());
        }

        [TestClass]
        public class CaseSensitive : StringTokenTest
        {
            StringToken a = new StringToken("a");
            StringToken a2 = new StringToken("a");
            StringToken A = new StringToken("A");
            StringToken B = new StringToken("B");

            [TestMethod]
            public void Same_Word_Same_Case() => TestEqual(a, a2);

            [TestMethod]
            public void Same_Word_Different_Case() => TestNotEqual(a, A);

            [TestMethod]
            public void Different_Word() => TestNotEqual(a, B);
        }

        [TestClass]
        public class CaseInsensitive : StringTokenTest
        {
            StringToken a = new StringToken("a", true);
            StringToken a2 = new StringToken("a", true);
            StringToken A = new StringToken("A", true);
            StringToken B = new StringToken("B", true);

            [TestMethod]
            public void Same_Word_Same_Case() => TestEqual(a, a2);

            [TestMethod]
            public void Same_Word_Different_Case() => TestEqual(a, A);

            [TestMethod]
            public void Different_Word() => TestNotEqual(a, B);
        }
    }
}
