﻿using Dashboard.Infrastructure.Queries;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Infrastructure.Queries
{
    [TestClass]
    public class QueryDispatcherTest
    {
        [TestMethod]
        public void Can_Dispatch_Query()
        {
            var handler = new Mock<IQueryHandler<TestQuery, BaseTestClass>>();
            handler.Setup(x => x.Execute(It.IsAny<TestQuery>())).Returns(new BaseTestClass());

            Mock<IServiceProvider> serviceProvider = new Mock<IServiceProvider>();
            serviceProvider.Setup(x => x.GetService(typeof(IQueryHandler<TestQuery, BaseTestClass>)))
                .Returns(handler.Object);
            
            var dispatcher = new QueryDispatcher(serviceProvider.Object);
            var result = dispatcher.Dispatch(new TestQuery());

            result.Should().NotBeNull();
        }

        [TestMethod]
        public async Task Can_Dispatch_Query_Async()
        {
            var handler = new Mock<IAsyncQueryHandler<TestAsyncQuery, BaseTestClass>>();
            handler.Setup(x => x.ExecuteAsync(It.IsAny<TestAsyncQuery>())).Returns(Task.FromResult(new BaseTestClass()));

            Mock<IServiceProvider> serviceProvider = new Mock<IServiceProvider>();
            serviceProvider.Setup(x => x.GetService(typeof(IAsyncQueryHandler<TestAsyncQuery, BaseTestClass>)))
                .Returns(handler.Object);

            var dispatcher = new QueryDispatcher(serviceProvider.Object);
            var result = await dispatcher.DispatchAsync(new TestAsyncQuery());

            result.Should().NotBeNull();
        }
    }
}
