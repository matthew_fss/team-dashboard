﻿using Dashboard.Infrastructure.Queries;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Infrastructure.Queries
{
    [TestClass]
    public class AsyncEnumerableQueryTest
    {
        private Test[] tests = new Test[]
        {
            new Test()
            {
                Prop1 = 1,
                Prop2 = "1",
                Prop3 = true
            },
            new Test()
            {
                Prop1 = 2,
                Prop2 = "2",
                Prop3 = false
            }
        };

        public AsyncEnumerableQueryTest()
        {
            
        }

        [TestMethod]
        public async Task Performs_An_ExecuteQuery_Linq_Statement()
        {
            var first = await new AsyncEnumerableQuery<Test>(tests).FirstAsync();

            first.Should().Be(tests[0]);
        }

        [TestMethod]
        public async Task Combining_CreateQuery_With_ExecuteQuery()
        {
            var prop = await new AsyncEnumerableQuery<Test>(tests).Where(x => x.Prop1 == 2).Select(x => x.Prop2).FirstAsync();

            prop.Should().Be("2");
        }

        private class Test
        {
            public int Prop1 { get; set; }
            public string Prop2 { get; set; }
            public bool Prop3 { get; set; }
        }
    }
}
