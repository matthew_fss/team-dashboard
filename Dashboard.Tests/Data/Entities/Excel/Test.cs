﻿using Dashboard.Data.Entities.Excel;
using LinqToExcel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dashboard.Tests.Data.Entities.Efactorycel
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestMethod()
        {
            IEnumerable<TimeSheetBlock> l = null;
            using (var factory = new ExcelQueryFactory(@"\\fss\share\TimeTracking\2016\October 2016 - MAC.xlsx"))
            {
                factory.AddMapping<TimeSheetBlock>(x => x.CustomerCode, "Cust");
                factory.AddMapping<TimeSheetBlock>(x => x.ProjectCode, "Project");
                factory.AddMapping<TimeSheetBlock>(x => x.Description, "Description");
                factory.AddMapping<TimeSheetBlock>(x => x.JiraTicket, "JIRA Ticket");
                factory.AddMapping<TimeSheetBlock>(x => x.TimeOfDay, "Time");
                factory.ReadOnly = true;
                
                factory.AddTransformation<TimeSheetBlock>(x => x.TimeOfDay, time =>
                 {
                     return TimeSpan.Parse(time);
                 });

                var y = Enumerable
                    .Range(1, DateTime.DaysInMonth(2016, 10))
                    .Select(i => factory.WorksheetRange<TimeSheetBlock>("E1", "I73", $"{i}{GetDaySuffix(i)}"));


                l = y.SelectMany(x => x).Where(x => x.JiraTicket != null);

                var t = 0;
            }

            var m = l.ToList();
        }

        private string GetDaySuffix(int day)
        {
            return day % 10 == 1 && day != 11
                ? "st"
                : day % 10 == 2 && day != 12
                    ? "nd"
                    : day % 10 == 3 && day != 13
                        ? "rd"
                        : "th";
        }
    }
}
