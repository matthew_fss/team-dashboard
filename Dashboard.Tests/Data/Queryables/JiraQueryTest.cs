﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Moq;
using FluentAssertions;
using System.Data.Entity;
using Dashboard.Data.Domain.Queryables.Jira;
using Dashboard.Data.Domain.Jira;
using RestSharp;
using Dashboard.Tests.Helpers;

namespace Dashboard.Tests.Data.Queryables
{
    [TestClass]
    public class PagedJiraQueryTest
    {
        private Mock<IRestClient> client = new Mock<IRestClient>();
        private const string resource = "test/resource";

        private int maxResults = 4;
        private IList<TestResource> TestData;

        private PagedJiraQuery<TestResource> Query;
        public PagedJiraQueryTest()
        {
            List<TestResource> list = new List<TestResource>();
            Random rand = new Random();
            int total = rand.Next(2, 20);
            for(int i = 0; i < total; i++)
            {
                list.Add(new TestResource()
                {
                    Prop1 = rand.Next(),
                    Prop2 = rand.Next() % 2 == 0,
                    Prop3 = Generate(rand.Next(3, 9))
                });
            }

            TestData = list;

            Task<IRestResponse<PagedResult<TestResource>>>[] pages = new Task<IRestResponse<PagedResult<TestResource>>>[(int)Math.Ceiling(list.Count / (decimal)maxResults)];
            for(int i = 0; i < pages.Length; i++)
            {
                var resources = TestData.Skip(i * maxResults).Take(maxResults);
                var result = new PagedResult<TestResource>()
                {
                    IsLast = i == (pages.Length - 1),
                    MaxResults = maxResults,
                    StartAt = i * maxResults,
                    Total = resources.Count(),
                    Values = resources,
                };

                pages[i] = Task.FromResult(new RestResponse<PagedResult<TestResource>>()
                {
                    Data = result,
                    StatusCode = System.Net.HttpStatusCode.OK
                } as IRestResponse<PagedResult<TestResource>>);
            }

            client.Setup(x => x.ExecuteTaskAsync<PagedResult<TestResource>>(It.Is<IRestRequest>(req => req.Resource.StartsWith(resource))))
                .ReturnsInOrder(pages);

            Query = new PagedJiraQuery<TestResource>(resource, client.Object);
        }

        [TestMethod]
        public async Task Returns_All_Data_From_GetAsync()
        {
            var list = (await Query.GetAsync()).ToList();

            list.Should().HaveCount(TestData.Count());

            for(int i = 0; i < list.Count; i++)
            {
                list[i].ShouldBeEquivalentTo(TestData[i]);
            }
        }

        [TestMethod]
        public async Task Gets_Data_From_An_Execute_Linq_Statement()
        {
            var first = await Query.FirstOrDefaultAsync();

            first.Should().NotBeNull();

            first.ShouldBeEquivalentTo(TestData[0]);
        }

        [TestMethod]
        public async Task Combine_Execute_And_CreateQuery_Linq_Statement()
        {
            var prop = await Query.Where(x => x.Prop2 == false).Select(x => x.Prop1).FirstOrDefaultAsync();

            prop.Should().Be(TestData.Where(x => x.Prop2 == false).Select(x => x.Prop1).FirstOrDefault());
        }

        private string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private string Generate(int length)
        {
            Random rand = new Random();
            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < length; i++)
            {
                builder.Append(characters[rand.Next(characters.Length)]);
            }

            return builder.ToString();
        }
        private class TestResource
        {
            public int Prop1 { get; internal set; }
            public bool Prop2 { get; internal set; }
            public string Prop3 { get; internal set; }
        }
    }
}
