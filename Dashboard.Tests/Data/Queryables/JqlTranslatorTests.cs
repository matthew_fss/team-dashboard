﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Dashboard.Data.Models.Jira;
using Dashboard.Data.Queryables;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Data.Queryables
{
    [TestClass]
    public class JqlTranslatorTests
    {
        [TestClass]
        public class Querying_Properties : JqlTranslatorTests
        {
            [TestMethod]
            public void Assignee() => TestQuery(query => query.Where(x => x.Assignee == "Developer"), "Assignee = Developer");
        }
        
        [TestClass]
        public class Querying_Operators : JqlTranslatorTests
        {
            [TestMethod]
            public void Equals() => TestQuery(query => query.Where(x => x.Assignee == "Developer"), "Assignee = Developer");

            [TestMethod]
            public void Equals_Null() => TestQuery(query => query.Where(x => x.Assignee == null), "Assignee is empty");
            
            [TestMethod]
            public void NotEquals() => TestQuery(query => query.Where(x => x.Assignee != "Tim"), "Assignee != Tim");

            [TestMethod]
            public void NotEquals_Null() => TestQuery(query => query.Where(x => x.Assignee != null), "Assignee is not empty");

            [TestMethod]
            public void And() => TestQuery(query => query.Where(x => x.Assignee == "Matthew" && x.Project == "TST"), "(Assignee = Matthew AND Project = TST)");

            [TestMethod]
            public void Or() => TestQuery(query => query.Where(x => x.Project == "CD" || x.Assignee == "Person"), "(Project = CD OR Assignee = Person)");

            [TestMethod]
            public void Not() => TestQuery(query => query.Where(x => !(x.Assignee == null)), "NOT (Assignee is empty)");
        }

        private void TestQuery(Func<IssueQuery, IQueryable<Issue>> queryBuilder, string expected)
        {
            var translator = new JqlTranslator();
            var query = queryBuilder(new IssueQuery());

            translator.Translate(query.Expression).Should().Be(expected);
        }
    }
}
