﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Dashboard.Data.Domain.Jql;
using Dashboard.Data.Domain.Queryables;
using Dashboard.Data.Entities.Jira;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Data.Domain.Jql
{
    [TestClass]
    public class JqlQueryTranslatorTests
    {
        [TestClass]
        public class Querying_Properties : JqlQueryTranslatorTests
        {
            [TestMethod]
            public void Key()
            {
                QueryTestingHelpers.TestQuery(query => query.Where(x => x.Key > "TST-3"), "Key > TST-3");
                QueryTestingHelpers.TestQuery(query => query.Where(x => x.Key <= "ASDFASDF"), "Key <= ASDFASDF");
                QueryTestingHelpers.TestQuery(query => query.Where(x => x.Key == "AN OTHER"), "Key = \"AN OTHER\"");
            }

            [TestMethod]
            public void Project()
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Project == "TEST"), "Project = TEST");

            [TestMethod]
            public void Assignee()
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Assignee == "Developer"), "Assignee = Developer");

            [TestMethod]
            public void Type()
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Type == "Test"), "Type = Test");

            [TestMethod]
            public void Status()
            {
                QueryTestingHelpers.TestQuery(query => query.Where(x => x.Status == "Another Test"), "Status = \"Another Test\"");
            }

            [TestMethod]
            public void Created()
            {
                QueryTestingHelpers.TestQuery(query => query.Where(x => x.Created > DateTime.Parse("July 30, 2016")), "Created > 07/30/2016");
                QueryTestingHelpers.TestQuery(query => query.Where(x => x.Created == null), "Created is empty");
            }

            [TestMethod]
            public void Summary()
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Summary == "Test"), "Summary = Test");

            [TestMethod]
            public void Description()
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Description.Contains("Test")), "Description ~ Test");
            
            [TestMethod]
            public void Epic()
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Epic == "Test"), "\"Epic Link\" = Test");

            [TestMethod]
            public void Reporter()
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Reporter == "Mr. Sammy"), "Reporter = \"Mr. Sammy\"");

            [TestMethod]
            public void OriginalEstimateInSeconds()
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.OriginalEstimateInSeconds > 120), "OriginalEstimate > 2");
        }
        
        [TestClass]
        public class Querying_Operators : JqlQueryTranslatorTests
        {
            [TestMethod]
            public void Equals() 
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Assignee == "Developer"), "Assignee = Developer");

            [TestMethod]
            public void Equals_Null() 
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Assignee == null), "Assignee is empty");
            
            [TestMethod]
            public void NotEquals() 
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Assignee != "Tim"), "Assignee != Tim");

            [TestMethod]
            public void NotEquals_Null() 
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Assignee != null), "Assignee is not empty");

            [TestMethod]
            public void And() 
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Assignee == "Matthew" && x.Project == "TST"), "(Assignee = Matthew AND Project = TST)");

            [TestMethod]
            public void Or() 
                => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Project == "CD" || x.Assignee == "Person"), "(Project = CD OR Assignee = Person)");

            [TestMethod]
            public void Not() 
                => QueryTestingHelpers.TestQuery(query => query.Where(x => !(x.Assignee == null)), "NOT (Assignee is empty)");

            [TestClass]
            public class Contains
            {
                [TestMethod]
                public void Collections()
                {
                    QueryTestingHelpers.TestQuery(query => query.Where(x => new string[] { "One", "Two" }.Contains(x.Status)), "Status in (One, Two)");

                    var array = new string[] { "One", "Two" };
                    QueryTestingHelpers.TestQuery(query => query.Where(x => array.Contains(x.Status)), "Status in (One, Two)");

                    QueryTestingHelpers.TestQuery(query => query.Where(x => new List<string>() { "One", "Two" }.Contains(x.Status)), "Status in (One, Two)");

                    var list = new List<string>() { "One", "Two" };
                    QueryTestingHelpers.TestQuery(query => query.Where(x => list.Contains(x.Status)), "Status in (One, Two)");
                }

                [TestMethod]
                public void Not_Collections()
                {
                    QueryTestingHelpers.TestQuery(query => query.Where(x => !new string[] { "One", "Two" }.Contains(x.Status)), "Status not in (One, Two)");

                    var array = new string[] { "One", "Two" };
                    QueryTestingHelpers.TestQuery(query => query.Where(x => !array.Contains(x.Status)), "Status not in (One, Two)");

                    QueryTestingHelpers.TestQuery(query => query.Where(x => !new List<string>() { "One", "Two" }.Contains(x.Status)), "Status not in (One, Two)");

                    var list = new List<string>() { "One", "Two" };
                    QueryTestingHelpers.TestQuery(query => query.Where(x => !list.Contains(x.Status)), "Status not in (One, Two)");
                }

                [TestMethod]
                public void Strings()
                {
                    QueryTestingHelpers.TestQuery(query => query.Where(x => x.Description.Contains("Special Test String")), "Description ~ \"Special Test String\"");
                    QueryTestingHelpers.TestQuery(query => query.Where(x => !x.Epic.Contains("Key")), "\"Epic Link\" !~ Key");
                }
            }
        }
        
        [TestClass]
        public class Querying_Methods
        {
            [TestClass]
            public class Include
            {
                [TestMethod]
                public void Default_To_Fulcrum_Fields()
                    => QueryTestingHelpers.TestQuery(query => query, JqlField.Fulcrum);

                [TestMethod]
                public void Include_Only_One_Field()
                    => QueryTestingHelpers.TestQuery(query => query.Include(JqlField.Summary), JqlField.Summary);

                [TestMethod]
                public void Include_Many_Fields_Using_Bitwise_Or()
                    => QueryTestingHelpers.TestQuery(query => query.Include(JqlField.Summary | JqlField.Description | JqlField.Attachments), JqlField.Summary | JqlField.Description | JqlField.Attachments);
            }

            [TestClass]
            public class Combining_Methods
            {
                [TestMethod]
                public void Where_Then_Include()
                    => QueryTestingHelpers.TestQuery(query => query.Where(x => x.Status == "Test").Include(JqlField.Summary | JqlField.Description | JqlField.Attachments), 
                        new JqlQuery()
                        {
                            Fields = JqlField.Summary | JqlField.Description | JqlField.Attachments,
                            Query = "Status = Test"
                        });
            }
        }

        public static class QueryTestingHelpers
        {
            public static void TestQuery(Func<IssueQuery, IQueryable<Issue>> queryBuilder, string expected)
                => new JqlQueryTranslator()
                    .Translate(queryBuilder(new IssueQuery(new IssueQueryProvider(Mock.Of<IRestClient>()))).Expression)
                    .Query
                    .Should()
                    .Be(expected);

            public static void TestQuery(Func<IssueQuery, IQueryable<Issue>> queryBuilder, JqlField fields)
                => new JqlQueryTranslator()
                    .Translate(queryBuilder(new IssueQuery(new IssueQueryProvider(Mock.Of<IRestClient>()))).Expression)
                    .Fields
                    .Should()
                    .Be(fields);

            public static void TestQuery(Func<IssueQuery, IQueryable<Issue>> queryBuilder, JqlQuery query)
                => new JqlQueryTranslator()
                    .Translate(queryBuilder(new IssueQuery(new IssueQueryProvider(Mock.Of<IRestClient>()))).Expression)
                    .ShouldBeEquivalentTo(query);
        }
    }
}
