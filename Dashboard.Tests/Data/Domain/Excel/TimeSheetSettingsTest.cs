﻿using Dashboard.Data.Contracts.Excel;
using Dashboard.Data.Domain.Excel;
using Dashboard.Tests.Helpers;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;
using System.Linq;

namespace Dashboard.Tests.Data.Domain.Excel
{
    [TestClass]
    public class TimeSheetSettingsTest
    {
        [TestMethod]
        public void Validating_Basic_Settings()
        {
            var settings = new TimeSheetSettings(new MockClock(), new MockFileSystem());

            settings.ShouldBeEquivalentTo(new
            {
                TimeTrackingBasePath = @"\\fss\share\TimeTracking",
                CustomerColumnName = "Cust",
                JiraTicketColumnName = "JIRA Ticket",
                ProjectColumnName = "Project",
                TimeColumnName = "Time",
                TrackingEndCell = "I73",
                TrackingStartCell = "E1",
                DescriptionColumnName = "Description"
            });
        }

        [TestClass]
        public class TimeSheetFilePathMethod
        {
            private TimeSheetSettings settings;
            private MockFileSystem fileSystem = new MockFileSystem();

            public TimeSheetFilePathMethod()
            {
                settings = new TimeSheetSettings(
                    new MockClock().SetNow(new DateTime(2016, 4, 30)),
                    fileSystem
                );
            }

            [TestMethod]
            public void Exception_Thrown_For_Year_Before_2009()
            {
                Action action = () => settings.TimeSheetFilePath(2008, 1, "TEST");

                action.ShouldThrow<ArgumentOutOfRangeException>();
            }

            [TestMethod]
            public void Exception_Thrown_For_Year_After_The_Current_Clock_Year()
            {
                Action action = () => settings.TimeSheetFilePath(2017, 1, "TEST");

                action.ShouldThrow<ArgumentOutOfRangeException>();
            }

            [TestMethod]
            public void Exception_Thrown_A_Month_Value_NOT_Between_1_And_12()
            {
                Action action = () => settings.TimeSheetFilePath(2016, 0, "TEST");

                action.ShouldThrow<ArgumentOutOfRangeException>();

                action = () => settings.TimeSheetFilePath(2016, 13, "TEST");

                action.ShouldThrow<ArgumentOutOfRangeException>();
            }

            [TestMethod]
            public void Exception_Thrown_For_A_Year_That_Is_The_Same_As_Clock_Year_But_A_Month_After_Clock_Month()
            {
                Action action = () => settings.TimeSheetFilePath(2016, 6, "TEST");

                action.ShouldThrow<ArgumentOutOfRangeException>();
            }

            [TestMethod]
            public void Exception_Thrown_Because_File_Nor_Archive_File_Exists()
            {
                fileSystem.Setup(x => x.FileExists(It.IsAny<string>())).Returns(false);

                Action action = () => settings.TimeSheetFilePath(2016, 1, "TEST");

                action.ShouldThrow<InvalidOperationException>();
            }

            [TestMethod]
            public void The_Correct_TimeTracking_Path_Is_Returned()
            {
                fileSystem.Setup(x => x.FileExists(It.IsAny<string>())).Returns(true);

                var path = settings.TimeSheetFilePath(2016, 3, "TEST");

                path.Should().Be(@"\\fss\share\TimeTracking\2016\March 2016 - TEST.xlsx");
            }

            [TestMethod]
            public void The_Correct_Archive_TimeTracking_Path_Is_Returned()
            {
                fileSystem.SetupSequence(x => x.FileExists(It.IsAny<string>()))
                    .Returns(false)
                    .Returns(true);

                var path = settings.TimeSheetFilePath(2016, 3, "TEST");

                path.Should().Be(@"\\fss\share\TimeTracking\2016\Archive\March 2016 - TEST.xlsx");
            }
        }

        [TestClass]
        public class WorksheetNameMethod
        {
            private TimeSheetSettings settings;

            public WorksheetNameMethod()
            {
                settings = new TimeSheetSettings(
                    new MockClock().SetNow(new DateTime(2016, 4, 30)),
                    new MockFileSystem()
                );
            }

            [TestMethod]
            public void By_Day_Throws_An_Exception_When_Day_Is_Less_Than_One()
            {
                Action action = () => settings.WorksheetName(0);

                action.ShouldThrow<ArgumentOutOfRangeException>();
            }

            [TestMethod]
            public void By_Day_Throws_An_Exception_When_Day_Is_Greater_Than_31()
            {
                Action action = () => settings.WorksheetName(32);

                action.ShouldThrow<ArgumentOutOfRangeException>();
            }

            [TestMethod]
            public void Returns_Correct_Day_Sheet()
            {
                var days = Enumerable.Range(1, 31).Select(x =>
                {
                    if (x % 10 == 1 && x != 11)
                        return $"{x}st";

                    if (x % 10 == 2 && x != 12)
                        return $"{x}nd";

                    if (x % 10 == 3 && x != 13)
                        return $"{x}rd";

                    return $"{x}th";
                }).ToArray();

                for (int i = 0; i < days.Length; i++)
                    settings.WorksheetName(i + 1).Should().Be(days[i]);
            }

            [TestMethod]
            public void Returns_Correct_Sheet_Using_TimeSheetPage_Enum()
            {
                settings.WorksheetName(TimeSheetPage.Allocated).Should().Be("A");
                settings.WorksheetName(TimeSheetPage.Overview).Should().Be("O");

                var days = Enumerable.Range(1, 31).Select(x =>
                {
                    if (x % 10 == 1 && x != 11)
                        return $"{x}st";

                    if (x % 10 == 2 && x != 12)
                        return $"{x}nd";

                    if (x % 10 == 3 && x != 13)
                        return $"{x}rd";

                    return $"{x}th";
                }).ToArray();

                for (int i = 0; i < days.Length; i++)
                    settings.WorksheetName((TimeSheetPage)(i + 1)).Should().Be(days[i]);
            }
        }
    }
}
