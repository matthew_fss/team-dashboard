﻿using System;
using Dashboard.Data.Contracts.Excel;
using Dashboard.Data.Domain.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dashboard.Infrastructure.Clock;
using Dashboard.Infrastructure.FileSystem;
using Dashboard.Tests.Helpers;

namespace Dashboard.Tests.Data.Domain.Excel
{
    [TestClass]
    public class TimeSheetFactoryTest
    {
        [TestMethod]
        public void TestMethod()
        {
            var factory = new TimeSheetFactory(TestExcelUser.Instance, new MockClock(), new TimeSheetSettings(new SystemClock(), new FileSystem()));

            var sheet = factory.ForMonth(10).ForYear(2016).Get();

            var x = 0;
        }
    }
}
