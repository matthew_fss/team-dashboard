﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Tests.Data
{
    public static class JiraTestData
    {
        //#region Projects
        //public static RemoteProject[] Projects = new RemoteProject[]
        //{
        //    new RemoteProject() {
        //        id = "1",
        //        lead = "Matthew",
        //        description = "Project for testing number 1",
        //        key = "TS1",
        //        name = "Fun Test Project"
        //    },
        //    new RemoteProject() {
        //        id = "2",
        //        lead = "Not Matthew",
        //        description = "Project for testing number 2",
        //        key = "TS2",
        //        name = "Another Test Project"
        //    }
        //};

        //public static void Validate(Project jira, Dashboard.Data.Entities.Jira.Project us)
        //{
        //    us.Id.Should().Be(jira.Id);
        //    us.Key.Should().Be(jira.Key);
        //    us.Lead.Should().Be(jira.Lead);
        //    us.Name.Should().Be(jira.Name);
        //}
        //#endregion

        //#region Issues
        //public static RemoteIssue[] Issues = new RemoteIssue[]
        //{
        //    new RemoteIssue {
        //        id = "3",
        //        assignee = "One",
        //        project = "TS1",
        //        description = "Do something awesome",
        //        key = "TS1-3",
        //        type = new RemoteIssueType() { name = "Task" },
        //        summary = "Awesome number 1"
        //    },
        //    new RemoteIssue {
        //        id = "4",
        //        assignee = "One",
        //        project = "TS1",
        //        description = "Issue for TS1 number 4",
        //        key = "TS1-4",
        //        type = new RemoteIssueType() { name = "Task" },
        //        summary = "TS1 Number 4"
        //    },
        //    new RemoteIssue {
        //        id = "5",
        //        assignee = "One",
        //        project = "TS2",
        //        description = "Issue for TS2 number 5",
        //        key = "TS2-5",
        //        type = new RemoteIssueType() { name = "Task" },
        //        summary = "TS2 Number 5"
        //    },
        //    new RemoteIssue {
        //        id = "6",
        //        assignee = "One",
        //        project = "TS2",
        //        description = "Issue for TS2 number 6",
        //        key = "TS2-6",
        //        type = new RemoteIssueType() { name = "Task" },
        //        summary = "TS2 Number 6"
        //    },
        //    new RemoteIssue {
        //        id = "7",
        //        assignee = "One",
        //        project = "TS2",
        //        description = "Issue for TS2 number 7",
        //        key = "TS2-7",
        //        type = new RemoteIssueType() { name = "Task" },
        //        summary = "TS2 Number 7"
        //    }
        //};

        //public static void Validate(Issue jira, Dashboard.Data.Entities.Jira.Issue us)
        //{
        //    us.Assignee.Should().Be(jira.Assignee);
        //    us.Description.Should().Be(jira.Description);
        //    us.Epic.Should().Be(jira.CustomFields["Epic Link"].Name);
        //    us.Key.Should().Be(jira.Key);
        //    us.Reporter.Should().Be(jira.Reporter);
        //    us.Status.Should().Be(jira.Status.Name);
        //    us.Summary.Should().Be(jira.Summary);
        //    us.Type.Should().Be(jira.Type.Name);
        //}
        //#endregion
    }
}
