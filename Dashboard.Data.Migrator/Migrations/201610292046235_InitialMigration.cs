namespace Dashboard.Data.Migrator
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Board",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JiraID = c.Int(nullable: false),
                        Name = c.String(),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Issue",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        RecordDate = c.DateTime(nullable: false),
                        JiraID = c.Int(nullable: false),
                        IssueType = c.String(),
                        SprintID = c.Guid(),
                        Epic = c.String(),
                        Assignee = c.String(),
                        Updated = c.DateTime(),
                        Status = c.String(),
                        Summary = c.String(),
                        Description = c.String(),
                        OriginalEstimate = c.Long(),
                        RemainingEstimate = c.Long(),
                        BoardID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Board", t => t.BoardID, cascadeDelete: true)
                .ForeignKey("dbo.Sprint", t => t.SprintID)
                .Index(t => t.SprintID)
                .Index(t => t.BoardID);
            
            CreateTable(
                "dbo.Sprint",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProjectID = c.Guid(nullable: false),
                        State = c.String(nullable: false, maxLength: 10),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Project", t => t.ProjectID, cascadeDelete: true)
                .Index(t => t.ProjectID);
            
            CreateTable(
                "dbo.Project",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        JiraID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        Lead = c.String(maxLength: 256),
                        BoardID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Board", t => t.BoardID)
                .Index(t => t.BoardID);
            
            CreateTable(
                "dbo.WorkLog",
                c => new
                    {
                        IssueID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        LogDate = c.DateTime(nullable: false),
                        TimeLogged = c.Time(nullable: false, precision: 7),
                        Issue_ID = c.Guid(),
                    })
                .PrimaryKey(t => new { t.IssueID, t.UserID, t.LogDate })
                .ForeignKey("dbo.Issue", t => t.IssueID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .ForeignKey("dbo.Issue", t => t.Issue_ID)
                .Index(t => t.IssueID)
                .Index(t => t.UserID)
                .Index(t => t.Issue_ID);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserName = c.String(maxLength: 256),
                        EmailAddress = c.String(maxLength: 256),
                        Name = c.String(),
                        ExcelCode = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.UserName, unique: true)
                .Index(t => t.EmailAddress, unique: true)
                .Index(t => t.ExcelCode, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkLog", "Issue_ID", "dbo.Issue");
            DropForeignKey("dbo.WorkLog", "UserID", "dbo.User");
            DropForeignKey("dbo.WorkLog", "IssueID", "dbo.Issue");
            DropForeignKey("dbo.Issue", "SprintID", "dbo.Sprint");
            DropForeignKey("dbo.Sprint", "ProjectID", "dbo.Project");
            DropForeignKey("dbo.Project", "BoardID", "dbo.Board");
            DropForeignKey("dbo.Issue", "BoardID", "dbo.Board");
            DropIndex("dbo.User", new[] { "ExcelCode" });
            DropIndex("dbo.User", new[] { "EmailAddress" });
            DropIndex("dbo.User", new[] { "UserName" });
            DropIndex("dbo.WorkLog", new[] { "Issue_ID" });
            DropIndex("dbo.WorkLog", new[] { "UserID" });
            DropIndex("dbo.WorkLog", new[] { "IssueID" });
            DropIndex("dbo.Project", new[] { "BoardID" });
            DropIndex("dbo.Sprint", new[] { "ProjectID" });
            DropIndex("dbo.Issue", new[] { "BoardID" });
            DropIndex("dbo.Issue", new[] { "SprintID" });
            DropTable("dbo.User");
            DropTable("dbo.WorkLog");
            DropTable("dbo.Project");
            DropTable("dbo.Sprint");
            DropTable("dbo.Issue");
            DropTable("dbo.Board");
        }
    }
}
