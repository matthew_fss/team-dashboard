﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.WebApi.Library.Models
{
    public class Project
    {
        public Guid ID { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Lead { get; set; }
    }
}
