﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dashboard.Infrastructure.DependencyInjection;
using Dashboard.Infrastructure.Queries;

namespace Dashboard.WebApi.Library
{
    public class DependencyConfiguration : Infrastructure.DependencyInjection.DependencyConfiguration
    {
        public override void Configure(IDependencyInjector injector)
            => injector
                .TransientType<IQueryDispatcher, Dispatchers.QueryDispatcher>()
                .Register(Registration.ForOpenGeneric(typeof(IQueryHandler<,>), new[] { GetType().Assembly }).WithLifeCycle(LifeCycle.Transient));
    }
}
