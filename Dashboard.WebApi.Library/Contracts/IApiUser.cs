﻿using Dashboard.Data.Contracts;
using Dashboard.Data.Contracts.Database;
using Dashboard.Data.Contracts.Excel;
using Dashboard.Data.Contracts.Jira;

namespace Dashboard.WebApi.Library.Contracts
{
    public interface IApiUser : IJiraUser, IExcelUser, IDbUser
    {
        string Name { get; }
    }
}
