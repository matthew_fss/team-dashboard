﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dashboard.Infrastructure.Queries;
using Dashboard.WebApi.Library.Contracts;

namespace Dashboard.WebApi.Library.Dispatchers
{
    public class QueryDispatcher : Infrastructure.Queries.QueryDispatcher
    {
        private IApiUser _user;
        public QueryDispatcher(
            IApiUser user,
            IServiceProvider provider
        ) : base(provider)
        {
            _user = user;
        }

        public override TResult Dispatch<TResult>(IQuery<TResult> query)
        {
            return base.Dispatch<TResult>(query);
        }

        public override Task<TResult> DispatchAsync<TResult>(IAsyncQuery<TResult> query)
        {
            return base.DispatchAsync<TResult>(query);
        }
    }
}
