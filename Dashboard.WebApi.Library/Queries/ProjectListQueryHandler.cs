﻿using Dashboard.Data.Contracts;
using Dashboard.Infrastructure.Queries;
using Dashboard.WebApi.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dashboard.WebApi.Library.Queries
{
    public class ProjectListQuery : IQuery<IEnumerable<Project>> { }

    public class ProjectListQueryHandler : IQueryHandler<ProjectListQuery, IEnumerable<Project>>
    {
        private IStore<Data.Entities.Database.Project> _projectStore;

        public ProjectListQueryHandler(IStore<Data.Entities.Database.Project> projectStore)
        {
            _projectStore = projectStore;
        }
        public IEnumerable<Project> Execute(ProjectListQuery query)
        {
            return _projectStore.Entities.Select(x => new Project()
            {
                ID = x.ID,
                Lead = x.Lead,
                Name = x.Name
            });
        }
    }
}
