﻿using Dashboard.Infrastructure.DependencyInjection;
using Dashboard.TaskRunner.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.TaskRunner
{
    internal class RegisteredTask
    {
        public IDependencyInjector Injector { get; set; }

        public Type Task { get; set; }

        public bool IsGeneric => Task.GetInterfaces().Any(t => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(ITask<>));

        public ITaskRegistration Registration { get; set; }

        public async Task<object> ExecuteTaskAsync()
        {
            var task = Injector.Resolve(Task);

            if (IsGeneric)
                return await ((ITask<dynamic>)task).ExecuteAsync();

            await ((ITask)task).ExecuteAsync();
            return null;
        }
    }
}
