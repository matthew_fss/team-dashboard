﻿using Dashboard.Infrastructure.DependencyInjection;
using Dashboard.TaskRunner.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.TaskRunner
{
    public class TaskRunner
    {
        private IDictionary<Type, List<RegisteredTask>> task_registrations = new Dictionary<Type, List<RegisteredTask>>();
        private IDependencyInjector _injector;
        public TaskRunner(IDependencyInjector injector)
        {
            _injector = injector;
        }

        public void Register(Assembly fromAssembly) => Register(new[] { fromAssembly });

        public void Register(IEnumerable<Assembly> fromAssemblies)
            => Register(
                fromAssemblies.SelectMany(x => x.GetTypes())
                    .Where(t => t.IsClass && !t.IsAbstract && typeof(ITaskRegistration).IsAssignableFrom(t))
                    .Select(t => Activator.CreateInstance(t) as ITaskRegistration)
            );

        public void Register(ITaskRegistration registration) => Register(new[] { registration });

        public void Register(IEnumerable<ITaskRegistration> registrations)
        {
            foreach(ITaskRegistration registrar in registrations)
            {
                List<RegisteredTask> list;
                if(!task_registrations.TryGetValue(registrar.Task, out list))
                {
                    list = new List<RegisteredTask>();
                    task_registrations.Add(registrar.Task, list);
                }

                var injector = _injector.CreateChildInjector();
                registrar.RegisterDependencies(injector);

                list.Add(new RegisteredTask()
                {
                    Injector = injector,
                    Registration = registrar,
                    Task = registrar.Task
                });
            }
        }

        public Task Run(Type task)
        {
            return null;
        }
    }
}
