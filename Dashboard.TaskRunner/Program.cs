﻿using Dashboard.Data.Contracts.Database;
using Dashboard.Data.Entities.Database;
using Dashboard.Data.Stores.Database;
using Dashboard.Data.Stores.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RestSharp;
using Dashboard.Data.Domain.Authentication;
using Dashboard.Data.Entities.Jira;
using Dashboard.Data.Domain.Jira;

namespace Dashboard.TaskRunner
{
    class Program
    {
        private static string Username = "Matthew";
        private static IRestClient client = new RestClient(JiraApi.Host);

        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                try
                {
                    var authService = new JiraAuthenticationService(client);
                    JiraSession session;
                    var res = await authService.Authenticate("Matthew", "mmeasc#2536");

                    client.Authenticator = new JiraAuthenticator(res.Session);   
                    //Console.WriteLine("Starting Users Sync");
                    //await SyncUsers();
                    //Console.WriteLine("Finishing Users Sync");
                    //Console.WriteLine("Starting Boards Sync");
                    //await SyncBoards();
                    //Console.WriteLine("Finishing Boards Sync");
                    //Console.WriteLine("Starting Projects Sync");
                    //await SyncProjects();
                    //Console.WriteLine("Finishing Projects Sync");
                    Console.WriteLine("Starting Issues Sync");
                    await SyncIssues();
                    Console.WriteLine("Finishing Issues Sync");
                }
                catch (Exception ex)
                {
                    throw;
                }
            }).Wait();
        }

        private static async Task SyncUsers()
        {
            var jiraUserStore = new UserStore(client);
            var dbUserStore = new DbStore<Data.Entities.Database.User>(null);

            foreach (Data.Entities.Jira.User jiraUser in jiraUserStore.Entities)
            {
                Data.Entities.Database.User user = await dbUserStore.Entities.SingleOrDefaultAsync(x => x.UserName == jiraUser.Name);

                if (user == null)
                {
                    user = dbUserStore.Add(new Data.Entities.Database.User()
                    {
                        ID = Guid.NewGuid()
                    });
                }

                user.EmailAddress = jiraUser.EmailAddress;
                user.Name = jiraUser.DisplayName;
                user.UserName = jiraUser.Name;
            }

            await dbUserStore.SaveChangesAsync();
        }

        private static async Task SyncBoards()
        {
            var jiraBoardStore = new BoardStore(client);
            var dbBoardStore = new DbStore<Data.Entities.Database.Board>(null);

            foreach (Data.Entities.Jira.Board jiraBoard in jiraBoardStore.Entities)
            {
                var jiraID = int.Parse(jiraBoard.Id);
                var dbBoard = await dbBoardStore.Entities.SingleOrDefaultAsync(x => x.JiraID == jiraID);

                if (dbBoard == null)
                {
                    dbBoard = dbBoardStore.Add(new Data.Entities.Database.Board()
                    {
                        ID = Guid.NewGuid(),
                        JiraID = jiraID
                    });
                }

                dbBoard.Name = jiraBoard.Name;
                dbBoard.Type = jiraBoard.Type;
            }

            await dbBoardStore.SaveChangesAsync();
        }

        private static async Task SyncProjects()
        {
            var jiraBoardStore = new BoardStore(client);
            var dbBoardStore = new DbStore<Data.Entities.Database.Board>(null);
            var dbProjectStore = new DbStore<Data.Entities.Database.Project>(null);

            foreach (Data.Entities.Jira.Board jiraBoard in jiraBoardStore.Entities)
            {
                int id = int.Parse(jiraBoard.Id);
                Data.Entities.Database.Board dbBoard = await dbBoardStore.Entities.SingleOrDefaultAsync(x => x.JiraID == id);

                foreach (Data.Entities.Jira.Project jiraProject in jiraBoard.Projects)
                {
                    int projectJiraID = int.Parse(jiraProject.Id);
                    Data.Entities.Database.Project project = await dbProjectStore.Entities.SingleOrDefaultAsync(x => x.JiraID == id);

                    if (project == null)
                    {
                        project = dbProjectStore.Add(new Data.Entities.Database.Project()
                        {
                            ID = Guid.NewGuid(),
                            JiraID = projectJiraID,
                            BoardID = dbBoard?.ID
                        });
                    }

                    project.Lead = jiraProject.Lead?.Name;
                    project.Name = jiraProject.Name;
                }

            }

            await dbProjectStore.SaveChangesAsync();
        }

        private static async Task SyncIssues()
        {
            var jiraBoardStore = new BoardStore(client);
            var dbBoardStore = new DbStore<Data.Entities.Database.Board>(null);
            var dbIssueStore = new DbStore<Data.Entities.Database.Issue>(null);

            try
            {
                foreach (Data.Entities.Jira.Board b in jiraBoardStore.Entities)
                {
                    int bID = int.Parse(b.Id);
                    var dbBoard = await dbBoardStore.Entities.SingleAsync(x => x.JiraID == bID);

                    try
                    {
                        foreach (Data.Domain.Jql.JiraApiIssue issue in b.Issues)
                        {
                            try
                            {
                                Console.WriteLine($"Board: {b.Name}, Issues: {issue.key} - {issue.fields.summary}");
                                int id = int.Parse(issue.id);
                                var dbIssue = await dbIssueStore.Entities.SingleOrDefaultAsync(x => x.JiraID == id);

                                if (dbIssue == null)
                                {
                                    dbIssue = dbIssueStore.Add(new Data.Entities.Database.Issue()
                                    {
                                        ID = Guid.NewGuid(),
                                        JiraID = id,
                                        BoardID = dbBoard.ID,
                                        RecordDate = DateTime.Now
                                    });
                                }

                                dbIssue.Assignee = issue.fields.assignee?.name;
                                dbIssue.Description = issue.fields.description;
                                dbIssue.Epic = issue.fields.epic?.name;
                                dbIssue.IssueType = issue.fields.issuetype?.name;
                                dbIssue.OriginalEstimate = issue.fields.timetracking != null ? (long?)new TimeSpan(0, 0, issue.fields.timetracking.originalEstimateSeconds).Ticks : null;
                                dbIssue.RemainingEstimate = issue.fields.timetracking != null ? (long?)new TimeSpan(0, 0, issue.fields.timetracking.remainingEstimateSeconds).Ticks : null;
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"Exception: {ex.Message}");
                            }
                        }

                        Console.WriteLine($"Saving Board {b.Name}");
                        await dbIssueStore.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Exception: {ex.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
            }
        }
    }

    public class DbUser : IDbUser
    {
        public Guid UserID { get; set; }

        public string UserName { get; set; }
    }
}
