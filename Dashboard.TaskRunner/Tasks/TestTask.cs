﻿using Dashboard.TaskRunner.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dashboard.Infrastructure.DependencyInjection;

namespace Dashboard.TaskRunner.Tasks
{
    public class TestTask : ITask
    {
        public Task ExecuteAsync()
        {
            throw new NotImplementedException();
        }
    }

    public class TestTaskRegistration : ITaskRegistration
    {
        public IEnumerable<Type> Required
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Type Task { get; } = typeof(TestTask);

        public void RegisterDependencies(IDependencyInjector injector)
        {
            throw new NotImplementedException();
        }
    }
}
