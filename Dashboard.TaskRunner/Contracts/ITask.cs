﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.TaskRunner.Contracts
{
    public interface ITask
    {
        Task ExecuteAsync();
    }

    public interface ITask<TResult>
    {
        Task<TResult> ExecuteAsync();
    }
}
