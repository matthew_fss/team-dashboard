﻿using Dashboard.Infrastructure.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.TaskRunner.Contracts
{
    public interface ITaskRegistration
    {
        /// <summary>
        /// The type of the task to register.
        /// </summary>
        /// <remarks>
        /// Must be a type of ITask or ITask<>
        /// </remarks>
        Type Task { get; }

        /// <summary>
        /// A list of required tasks that must be in a completed state,
        /// before this registered task is executed.
        /// </summary>
        /// <remarks>
        /// If the required task returns a value, its return type is registered
        /// into the dependency injector, so it can then be injected into this task.
        /// </remarks>
        IEnumerable<Type> Required { get; }

        void RegisterDependencies(IDependencyInjector injector);
    }
}
