﻿using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.WebApi.Domain.Authentication
{
    public class PasswordValidator : IResourceOwnerPasswordValidator
    {
        public Task<CustomGrantValidationResult> ValidateAsync(string userName, string password, ValidatedTokenRequest request)
        {
            return Task.FromResult(new CustomGrantValidationResult("tester", "password"));
        }
    }
}
