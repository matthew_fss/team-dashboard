﻿using Microsoft.Practices.ObjectBuilder2;
using System;

namespace Dashboard.WebApi.Domain.DependencyInjection
{
    public class EnumerableResolutionStrategy : IBuilderStrategy
    {
        public void PostBuildUp(IBuilderContext context) { }

        public void PostTearDown(IBuilderContext context) { }

        public void PreBuildUp(IBuilderContext context)
        {
            if (context.BuildKey.Type.IsGenericType && context.BuildKey.Type.FullName.StartsWith("System.Collections.Generic.IEnumerable"))
            {
                var arrayType = context.BuildKey.Type.GetGenericArguments()[0].MakeArrayType();
                context.BuildKey = new NamedTypeBuildKey(arrayType, context.BuildKey.Name);
            }
        }

        public void PreTearDown(IBuilderContext context) { }
    }
}