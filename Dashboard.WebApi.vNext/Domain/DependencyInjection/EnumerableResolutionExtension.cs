﻿using Microsoft.Practices.Unity;
namespace Dashboard.WebApi.Domain.DependencyInjection
{
    public class EnumerableResolutionExtension : UnityContainerExtension
    {
        protected override void Initialize() => Context.Strategies.Add(
            new EnumerableResolutionStrategy(),
            Microsoft.Practices.Unity.ObjectBuilder.UnityBuildStage.TypeMapping
        );
    }
}