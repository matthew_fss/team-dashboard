﻿using Dashboard.Data.Contracts.Jira;
using Dashboard.Data.Domain.Jira;
using Dashboard.Infrastructure.DependencyInjection;
using Dashboard.WebApi.Domain.Identity;
using System;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Dashboard.WebApi.Domain.Authentication
{
    public class JiraAuthorizationFilter : IAuthorizationFilter, IFilter
    {
        private IDependencyInjector _injector;
        public JiraAuthorizationFilter(IDependencyInjector injector)
        {
            _injector = injector;
        }

        public bool AllowMultiple { get; } = false;

        public async Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            var authService = _injector.Resolve<IJiraAuthenticationService>();
            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;
            Claim claim = principal.FindFirst(WebApiClaimType.JiraSession);

            if (!actionContext.AnonymousRequest())
            {
                if (!principal.Identity.IsAuthenticated || claim == null || !(await authService.ValidateSession(principal.Identity.Name, new JiraSession() { Token = claim.Value })))
                {
                    return actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                }
            }

            return await continuation();
        }
    }
}