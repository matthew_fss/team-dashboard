﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Dashboard.Data.Contracts;
using Dashboard.WebApi.Domain.Identity;
using Dashboard.Data.Entities.Jira;
using Dashboard.Data.Contracts.Jira;
using Dashboard.Data.Entities.Database;
using System.Data.Entity;
using Microsoft.AspNet.Identity;

namespace Dashboard.WebApi.Domain.Authentication
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private IJiraAuthenticationService _service;
        private IStore<Data.Entities.Database.User> _userStore;
        public ApplicationOAuthProvider(IJiraAuthenticationService service, IStore<Data.Entities.Database.User> userStore)
        {
            _service = service;
            _userStore = userStore;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var response = await _service.Authenticate(context.UserName, context.Password);

            if (!response.Authenticated)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            ClaimsIdentity identity = new ClaimsIdentity("Bearer");
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim(WebApiClaimType.JiraSession, response.Session.Token));

            var user = await _userStore.Entities.FirstOrDefaultAsync(x => x.UserName == context.UserName);
            
            if(user != null)
            {
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, $"{user.ID}"));
                identity.AddClaim(new Claim(WebApiClaimType.ExcelCode, user.ExcelCode));
            }

            AuthenticationProperties properties = CreateProperties("User");
            AuthenticationTicket ticket = new AuthenticationTicket(identity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(new AuthenticationProperties()
            {
                IsPersistent = true
            }, identity);

            await Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (true)//context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateAuthorizeRequest(OAuthValidateAuthorizeRequestContext context)
        {
            return base.ValidateAuthorizeRequest(context);
        }
        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }
    }
}