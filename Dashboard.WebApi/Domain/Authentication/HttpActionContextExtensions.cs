﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Dashboard.WebApi.Domain.Authentication
{
    public static class HttpActionContextExtensions
    {
        public static bool AnonymousRequest(this HttpActionContext context)
            => context.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
               || context.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
    }
}