﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;
using System.Security.Claims;
using Dashboard.Data.Contracts.Jira;
using Dashboard.Data.Domain.Jira;
using Owin;

namespace Dashboard.WebApi.Domain.Authentication
{
    public class JiraAuthenticationOptions
    {
        public string JiraClaimKey { get; set; }
    }

    public class JiraAuthenticationMiddleware : OwinMiddleware
    {
        private JiraAuthenticationOptions _options;
        private IDependencyResolver _resolver;

        public JiraAuthenticationMiddleware(OwinMiddleware next, JiraAuthenticationOptions options, IDependencyResolver resolver) 
            : base(next)
        {
            _options = options;
            _resolver = resolver;
        }

        public override async Task Invoke(IOwinContext context)
        {
            var authService = _resolver.GetService(typeof(IJiraAuthenticationService)) as IJiraAuthenticationService;
            var principal = (context.Request.User as ClaimsPrincipal);
            var username = principal?.Identity.Name;
            var session = principal?.FindFirst(_options.JiraClaimKey)?.Value;

            if (principal == null || authService == null || session == null || !await authService.ValidateSession(username, new JiraSession()
            {
                Token = session
            }))
                context.Request.User = null;

            await Next.Invoke(context);
        }
    }

    public static class JiraAuthenticationMiddlewareExtensions
    {
        public static IAppBuilder UseJiraAuthentication(this IAppBuilder app, JiraAuthenticationOptions options, IDependencyResolver resolver)
            => app.Use<JiraAuthenticationMiddleware>(options, resolver);
    }
}