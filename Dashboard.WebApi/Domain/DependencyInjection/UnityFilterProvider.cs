﻿using Dashboard.Infrastructure.DependencyInjection;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Dashboard.WebApi.Domain.DependencyInjection
{
    public class UnityFilterProvider : IFilterProvider
    {
        private UnityDependencyInjector _injector;
        private readonly ActionDescriptorFilterProvider _defaultProvider = new ActionDescriptorFilterProvider();

        public UnityFilterProvider(UnityDependencyInjector injector)
        {
            _injector = injector;
        }

        public IEnumerable<FilterInfo> GetFilters(HttpConfiguration configuration, HttpActionDescriptor actionDescriptor)
        {
            var filters = _defaultProvider.GetFilters(configuration, actionDescriptor);

            foreach (var filter in filters)
            {
                _injector.UnityContainer.BuildUp(filter.Instance.GetType(), filter.Instance);
            }

            return filters;
        }
    }
}