﻿using Dashboard.Infrastructure.DependencyInjection;
using Microsoft.Practices.Unity;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dashboard.WebApi.Domain.DependencyInjection
{
    public static class AppBuilderDependencyExtensions
    {
        private static string DependencyContainerKey = $"DependencyInjector_{Guid.NewGuid()}";

        public static IDependencyInjector SetInjector(this IAppBuilder app, IDependencyInjector injector)
            => SetInjector<IDependencyInjector>(app, injector);

        public static T SetInjector<T>(this IAppBuilder app, T injector)
            where T : IDependencyInjector
        {
            app.Properties[DependencyContainerKey] = injector;
            return injector;
        }

        public static IDependencyInjector GetInjector(this IAppBuilder app)
            => GetInjector<IDependencyInjector>(app);

        public static T GetInjector<T>(this IAppBuilder app)
            where T : IDependencyInjector
        {
            object obj;
            if (!(app.Properties.TryGetValue(DependencyContainerKey, out obj) && obj is T))
                throw new ArgumentException($"No injector of type {typeof(T)} was found.");

            return (T)obj;
        }
    }
}