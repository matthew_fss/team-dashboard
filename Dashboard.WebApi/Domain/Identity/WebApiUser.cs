﻿using System.Security.Claims;
using Microsoft.Owin;
using Dashboard.WebApi.Library.Contracts;
using System;
using Dashboard.Data.Domain.Jira;

namespace Dashboard.WebApi.Domain.Identity
{
    public class WebApiUser : IApiUser
    {
        private IOwinContext _context;
        public WebApiUser(IOwinContext context)
        {
            _context = context;
        }
        private ClaimsIdentity Identity
        {
            get { return _context.Request.User.Identity as ClaimsIdentity; }
        }

        public string Name => Identity?.FindFirst(ClaimTypes.Name)?.Value;

        public JiraSession? Session
        {
            get
            {
                string token = Identity?.FindFirst(WebApiClaimType.JiraSession)?.Value;

                return token != null ? (JiraSession?)new JiraSession() { Token = token } : null;
            }
        }

        public string UserName => Identity?.FindFirst(ClaimTypes.Name)?.Value;

        public string ExcelCode => Identity.FindFirst(WebApiClaimType.ExcelCode).Value;

        public Guid UserID => Guid.Parse(Identity.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}