﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dashboard.WebApi.Domain.Identity
{
    public static class WebApiClaimType
    {
        public const string JiraSession = "JiraSession";
        public const string ExcelCode = "ExcelCode";
    }
}