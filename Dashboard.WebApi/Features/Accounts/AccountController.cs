﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dashboard.WebApi.Features.Accounts
{
    [RoutePrefix("account")]
    public class AccountController : ApiController
    {
        [HttpGet]
        [Route("something")]
        public async Task<IHttpActionResult> Get()
        {
            return await Task.FromResult(Ok("Hi"));
        }
    }
}
