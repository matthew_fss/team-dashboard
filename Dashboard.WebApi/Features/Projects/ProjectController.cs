﻿using Dashboard.WebApi.Library.Queries;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dashboard.WebApi.Features.Projects
{
    public class ProjectController : BaseController
    {
        [HttpGet]
        [Route("projects")]
        public IHttpActionResult GetAll()
        {
            return Ok(QueryDispatcher.Dispatch(new ProjectListQuery()));
        }

        [HttpGet]
        [Route("project/{key}")]
        public async Task<IHttpActionResult> Get(string key)
        {
            return null;
        }

        [HttpGet]
        [Route("project/{projectKey}/issue")]
        public async Task<IHttpActionResult> GetIssues(string projectKey)
        {
            return null;
        }

        [HttpGet]
        [Route("project/{projectKey}/issue/{issueNumber}")]
        public async Task<IHttpActionResult> GetIssue(string projectKey, int issueNumber)
        {
            return null;
        }
    }
}
