﻿using Dashboard.Infrastructure.Commands;
using Dashboard.Infrastructure.Queries;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Dashboard.WebApi.Features
{
    public class BaseController : ApiController
    {
        [Microsoft.Practices.Unity.Dependency]
        public Infrastructure.Queries.IQueryDispatcher QueryDispatcher { get; set; }

        //[Dependency]
        //public ICommandDispatcher CommandDispatcher { get; set; }
    }
}
