﻿using Dashboard.Data.Contracts;
using Dashboard.Data.Contracts.Database;
using Dashboard.Data.Contracts.Excel;
using Dashboard.Data.Contracts.Jira;
using Dashboard.Data.Domain.Authentication;
using Dashboard.Data.Domain.Jira;
using Dashboard.Data.Entities.Database;
using Dashboard.Data.Entities.Jira;
using Dashboard.Data.Stores;
using Dashboard.Data.Stores.Jira;
using Dashboard.Infrastructure.Commands;
using Dashboard.Infrastructure.DependencyInjection;
using Dashboard.Infrastructure.Queries;
using Dashboard.WebApi.Domain.Authentication;
using Dashboard.WebApi.Domain.DependencyInjection;
using Dashboard.WebApi.Domain.Identity;
using Dashboard.WebApi.Library.Contracts;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Practices.Unity;
using Owin;
using RestSharp;
using System;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http.Dependencies;

namespace Dashboard.WebApi
{
    public static class Dependency
    {
        public static IAppBuilder ConfigureDependencies(this IAppBuilder app)
        {
            var injector = new UnityDependencyInjector();
            app.SetInjector(injector)
                .SingletonInstance<IServiceProvider>(injector)
                .SingletonInstance<IDependencyResolver>(injector)
                .SingletonFactory<IOAuthAuthorizationServerProvider>(di =>
                    new ApplicationOAuthProvider(new JiraAuthenticationService(new RestClient(JiraApi.Host)), new Data.Stores.Database.DbStore<Data.Entities.Database.User>(null))
                )
                .SingletonInstance<IRestClient>(new RestClient(JiraApi.Host))
                .TransientType<IJiraAuthenticationService, JiraAuthenticationService>()
                .TransientType<IAuthenticationService, JiraAuthenticationService>()

                .TransientFactory<IOwinContext>(di => HttpContext.Current.GetOwinContext())
                .Register(
                    new Registration(typeof(WebApiUser))
                        .ForTypes(new[] {
                            typeof(IApiUser),
                            typeof(IJiraUser),
                            typeof(IDbUser),
                            typeof(IExcelUser)
                        })
                );

            new Data.DependencyConfiguration().Configure(injector);
            new Library.DependencyConfiguration().Configure(injector);
            
            return app;
        }
    }
}