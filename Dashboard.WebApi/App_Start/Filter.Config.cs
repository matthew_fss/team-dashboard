﻿using Owin;
using System.Web;
using System.Web.Mvc;

namespace Dashboard.WebApi
{
    public static class Filter
    {
        public static IAppBuilder ConfigureFilters(this IAppBuilder app)
        {
            GlobalFilters.Filters.Add(new HandleErrorAttribute());

            return app;
        }
    }
}
