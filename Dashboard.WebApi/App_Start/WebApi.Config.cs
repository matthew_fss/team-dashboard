﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Owin;
using Dashboard.WebApi.Domain.DependencyInjection;
using Microsoft.Owin.Cors;
using Dashboard.WebApi.Domain.Authentication;
using System.Web.Http.Filters;
using Dashboard.Data.Contracts;
using System.Web.Http.Dependencies;
using Dashboard.Infrastructure.DependencyInjection;

namespace Dashboard.WebApi
{
    public static class WebApi
    {
        public static IAppBuilder ConfigureWebApi(this IAppBuilder app)
        {
            var config = new HttpConfiguration();

            // Web API routes
            config.MapHttpAttributeRoutes();

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.Filters.Add(new AuthorizeAttribute());
            //config.Filters.Add(new JiraAuthorizationFilter(app.GetInjector()));

            config.DependencyResolver = app.GetInjector().Resolve<IDependencyResolver>();
            
            var providers = config.Services.GetFilterProviders();
            var defaultprovider = providers.Single(i => i is ActionDescriptorFilterProvider);
            config.Services.Remove(typeof(IFilterProvider), defaultprovider);

            //TODO: IFilterProvider needs to be agnostic to Injector being used.
            config.Services.Add(typeof(IFilterProvider), new UnityFilterProvider(app.GetInjector<UnityDependencyInjector>()));

            var jsonSettings = config.Formatters.JsonFormatter.SerializerSettings;
            jsonSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            jsonSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            app.UseWebApi(config);
            app.UseCors(CorsOptions.AllowAll);

            return app;
        }
    }
}
