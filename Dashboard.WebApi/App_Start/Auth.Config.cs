﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Dashboard.WebApi.Domain.Authentication;
using Dashboard.WebApi.Domain.DependencyInjection;
using Microsoft.Owin.Security;
using Dashboard.Data.Domain.Authentication;
using Dashboard.Data.Domain.Jira;
using Dashboard.WebApi.Domain.Identity;
using System.Web.Http.Dependencies;

namespace Dashboard.WebApi
{
    public static class Auth
    {
        public static IAppBuilder ConfigureAuth(this IAppBuilder app)
        {
            
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/auth/token"),
                Provider = app.GetInjector().Resolve<IOAuthAuthorizationServerProvider>(),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            });
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            app.UseJiraAuthentication(new JiraAuthenticationOptions()
            {
                JiraClaimKey = WebApiClaimType.JiraSession
            }, app.GetInjector().Resolve<IDependencyResolver>());
            return app;
        }
    }
}
