﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Threading.Tasks;
using Microsoft.Owin.Cors;
using Dashboard.WebApi.Domain.DependencyInjection;

[assembly: OwinStartup(typeof(Dashboard.WebApi.Startup))]

namespace Dashboard.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll)
                .ConfigureDependencies()
                .ConfigureAuth()
                .Map("/api", builder => 
                {
                    builder.ConfigureWebApi();
                });
        }
    }
}
