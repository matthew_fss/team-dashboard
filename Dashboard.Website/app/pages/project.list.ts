﻿import { Component } from '@angular/core';
import { HttpClient } from '../http/httpclient';
import { NgFor } from '@angular/common';

@Component({
    templateUrl: 'app/pages/project.list.html',
    styleUrls: ['app/pages/project.css']
})
export class ProjectListComponent {
    private _projects = [];
    get projects() { return this._projects; } 

    constructor(http: HttpClient) {
        http.get('project').then(data => this._projects = data);
    }
}