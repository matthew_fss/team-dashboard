﻿import { Component, ViewEncapsulation, Injectable, provide } from '@angular/core';
import { FORM_DIRECTIVES } from '@angular/forms';

import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { Authentication } from '../services/authentication';

import { InputPlaceholder } from '../inputs/placeholder';
import { FulcrumLogo } from '../directives/logo';

import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/delay'

@Component({
    selector: 'login',
    templateUrl: 'app/pages/login.html',
    styleUrls: ['app/pages/login.css'],
    providers: [
        provide(RequestOptions, { useValue: new RequestOptions() }),
        Authentication
    ],
    directives: [FORM_DIRECTIVES, InputPlaceholder, FulcrumLogo],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent {
    public authenticating: boolean = false;
    constructor(private auth: Authentication, private router: Router) {
        console.log(auth);
    }

    signin(username: string, password: string) {
        this.authenticating = true;
        this.auth.login(username, password)
            .then((valid) => {
                if (valid)
                    this.router.navigate(['project'])
            })
            .catch((error) => console.log('Error'))
            .then(() => {
                this.authenticating = false
            });
    }
}