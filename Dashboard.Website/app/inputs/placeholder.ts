﻿import {
    Directive,
    Component,
    HostListener,
    ElementRef,
    Input,
    OnInit,
    OnChanges,
    DynamicComponentLoader,
    ViewContainerRef,
    ComponentRef,
    DoCheck,
    ViewChild,

    ReflectiveInjector,
    provide,

    trigger,
    state,
    transition,
    style,
    animate
} from '@angular/core';

import { NgIf } from '@angular/common';

@Directive({
    selector: 'input[type=text][placeholder], input[type=password][placeholder]'
})
export class InputPlaceholder implements OnChanges {
    get input(): HTMLInputElement {
        return this.element.nativeElement;
    }

    @Input('placeholder') placeholder;
    private container: Promise<ComponentRef<PlaceholderContainer>>;
    constructor(
        private element: ElementRef,
        private loader: DynamicComponentLoader,
        private view: ViewContainerRef
    ) {
        this.container = this.loader.loadNextToLocation(PlaceholderContainer, this.view, ReflectiveInjector.resolve([provide(InputPlaceholder, { useValue: this })]))
            .then(ref => ref.instance.update());
    }

    @HostListener('input')
    @HostListener('window:resize')
    updatePlaceholder() { }

    ngOnChanges() {
        this.input.placeholder = this.placeholder;
    }
}

@Component({
    selector: 'placeholder-container',
    template: `
        <span *ngIf="!!parent.input.value.length"
            [@animation]="'fadeIn'"
            [style.top.px]="parent.input.offsetTop + (parent.input.offsetHeight - height) / 2"
            [style.left.px]="parent.input.offsetLeft + parent.input.offsetWidth - width - offset"
            [style.zIndex]="zIndex"
        >
            {{parent.placeholder}}
        </span>
    `,
    styles: ["span { position: absolute; }"],
    animations: [
        trigger('animation', [
            state('fadeIn', style({ opacity: 1 })),
            transition('void => fadeIn', [
                style({ opacity: 0 }),
                animate(100)
            ]),
            transition('fadeIn => void', [
                style({ opacity: 1 }),
                animate(100, style({ opacity: 0 }))
            ])
        ])
    ]
})
class PlaceholderContainer {
    constructor(public parent: InputPlaceholder, public element: ElementRef) { }

    private height: number = 0;
    private width: number = 0;
    private zIndex: number = 0;
    private offset: number = 0;

    update() {
        let span = document.createElement('span');
        span.textContent = this.parent.placeholder;
        span.style.visibility = 'hidden';

        this.element.nativeElement.parentNode.insertBefore(span, this.element.nativeElement.nextElementSibling);
        this.height = span.offsetHeight;
        this.width = span.offsetWidth;
        this.offset = 5;
        this.element.nativeElement.parentNode.removeChild(span);

        this.zIndex = this.calculateZIndex(this.parent.input) + 1;
    }

    private calculateZIndex(element: Element) {
        if (!element)
            return 0;

        var z = parseInt(document.defaultView.getComputedStyle(element).getPropertyValue('z-index'));
        if (isNaN(z))
            return this.calculateZIndex(element.parentElement);

        return z;
    }
}

