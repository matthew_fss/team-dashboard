﻿
import { Component, ViewEncapsulation, provide } from '@angular/core';
import { Http } from '@angular/http';
import { RouterOutlet } from '@angular/router';
import { AppRequestOptions } from './http/apprequestoptions';
import { AuthToken } from './services/authentication';

@Component({
    selector: 'my-app',
    template: '<router-outlet></router-outlet>',
    directives: [RouterOutlet]
})
export class AppComponent { }