﻿import { Injectable, Inject } from '@angular/core';
import { RequestOptions, Headers, Http, RequestOptionsArgs } from '@angular/http';
import { AppConfig } from '../app.config';

import {Observable} from 'rxjs/Rx';
let token_key = "api_access_token";

export class AuthToken {
    toString() { return localStorage.getItem(token_key); }
}

@Injectable()
export class Authentication {
    constructor(private http: Http) {
        console.log(http);
    }
    login(username: string, password: string): Promise<boolean> {
        let header = new Headers();
        header.append("Content-Type", "application/x-www-form-urlencoded");
        return this.http.post(
            `${AppConfig.baseUrl}/auth/token`,
            `grant_type=password&username=${username}&password=${password}`, {
                headers: header
            })
            .map(res => res.json())
            .map(data => {
                localStorage.setItem(token_key, data.access_token);
                return true;
            }).toPromise();
    }
}