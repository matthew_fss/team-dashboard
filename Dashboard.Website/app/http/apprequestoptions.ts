﻿import { Injectable } from '@angular/core';
import { RequestOptions, RequestOptionsArgs, Headers } from '@angular/http';
import { AuthToken } from '../services/authentication';
import { AppConfig } from '../app.config';

@Injectable()
export class AppRequestOptions extends RequestOptions {
    constructor(token: AuthToken) {
        super();

        if (this.headers == null)
            this.headers = new Headers();

        this.headers.append('Content-Type', 'application/json');

        if (token)
            this.headers.append("Authorization", `Bearer ${token}`);
    }

    merge(options?: RequestOptionsArgs): RequestOptions {
        options.url = `${AppConfig.baseUrl}/api/${options.url}`;

        return super.merge(options);
    }
}