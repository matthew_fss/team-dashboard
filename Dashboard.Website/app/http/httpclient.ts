﻿import { Injectable } from '@angular/core';
import { Http, RequestMethod, Request, ResponseContentType, RequestOptionsArgs, RequestOptions } from '@angular/http';
import { Observable }  from 'rxjs/Observable';

@Injectable()
export class HttpClient {
    constructor(private http: Http, private defaultOptions: RequestOptions) { }

    get(resource: string): Promise<any>;
    get<T>(resource: string): Promise<T> {
        return this.makeRequest(RequestMethod.Get, resource)
    }

    post(resource: string, body: any): Promise<any>;
    post<T>(resource: string, body: any): Promise<T> {
        return this.makeRequest(RequestMethod.Post, resource, body);
    }

    put(resource: string, body: any): Promise<any>;
    put<T>(resource: string, body: any): Promise<T> {
        return this.makeRequest(RequestMethod.Put, resource, body);
    }

    delete(resource: string, body: any): Promise<any>;
    delete<T>(resource: string, body: any): Promise<T> {
        return this.makeRequest(RequestMethod.Delete, resource, body);
    }

    private makeRequest(method: RequestMethod, resource: string, body: any = null) {
        return this.http.request(resource, {
            method: method,
            body: body === undefined || body === null ? "" : body,
            responseType: ResponseContentType.Json
        })
        .map(res => res.json())
        .catch((error, caught) => {
            if (error.status == 401)
                console.log('redirect')

            return Observable.throw(`${error.status} - ${error.statusText}`);
        })
        .toPromise();
    }
}