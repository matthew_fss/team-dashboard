﻿import { NgModule, provide } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, RequestOptions } from '@angular/http';
import { ROUTER_DIRECTIVES, provideRouter } from '@angular/router';
import { disableDeprecatedForms, provideForms, FormsModule } from '@angular/forms';
import { router, routingProviders } from './app.routes';
import { AppRequestOptions } from './http/apprequestoptions';
import { HttpClient } from './http/httpclient';
import { AuthToken } from './services/authentication';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login';
import { ProjectListComponent } from './pages/project.list';

@NgModule({
    imports: [BrowserModule, FormsModule, router, HttpModule],
    providers: [
        routingProviders,
        provide(RequestOptions, { useClass: AppRequestOptions }),
        provide(HttpClient, { useClass: HttpClient }),
        provide(AuthToken, { useValue: new AuthToken() })
    ],
    declarations: [AppComponent, LoginComponent, ProjectListComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }