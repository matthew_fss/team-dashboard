﻿import {
    Component,
    ViewEncapsulation
} from '@angular/core'

@Component({
    selector: 'fulcrum-logo',
    templateUrl: 'app/directives/logo.html',
    styleUrls: ['app/directives/logo.css'],
    encapsulation: ViewEncapsulation.Native
})
export class FulcrumLogo {
}