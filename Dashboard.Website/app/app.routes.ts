﻿import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login';
import { ProjectListComponent } from './pages/project.list'

export var routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'projects', component: ProjectListComponent },
    { path: '**', redirectTo: 'login' }
];

export const routingProviders: any[] = [];
export const router = RouterModule.forRoot(routes);