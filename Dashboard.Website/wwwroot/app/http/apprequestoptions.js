"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var authentication_1 = require('../services/authentication');
var app_config_1 = require('../app.config');
var AppRequestOptions = (function (_super) {
    __extends(AppRequestOptions, _super);
    function AppRequestOptions(token) {
        _super.call(this);
        if (this.headers == null)
            this.headers = new http_1.Headers();
        this.headers.append('Content-Type', 'application/json');
        if (token)
            this.headers.append("Authorization", "Bearer " + token);
    }
    AppRequestOptions.prototype.merge = function (options) {
        options.url = app_config_1.AppConfig.baseUrl + "/api/" + options.url;
        return _super.prototype.merge.call(this, options);
    };
    AppRequestOptions = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [authentication_1.AuthToken])
    ], AppRequestOptions);
    return AppRequestOptions;
}(http_1.RequestOptions));
exports.AppRequestOptions = AppRequestOptions;
//# sourceMappingURL=apprequestoptions.js.map