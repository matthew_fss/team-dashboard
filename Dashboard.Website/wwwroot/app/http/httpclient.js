"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
var HttpClient = (function () {
    function HttpClient(http, defaultOptions) {
        this.http = http;
        this.defaultOptions = defaultOptions;
    }
    HttpClient.prototype.get = function (resource) {
        return this.makeRequest(http_1.RequestMethod.Get, resource);
    };
    HttpClient.prototype.post = function (resource, body) {
        return this.makeRequest(http_1.RequestMethod.Post, resource, body);
    };
    HttpClient.prototype.put = function (resource, body) {
        return this.makeRequest(http_1.RequestMethod.Put, resource, body);
    };
    HttpClient.prototype.delete = function (resource, body) {
        return this.makeRequest(http_1.RequestMethod.Delete, resource, body);
    };
    HttpClient.prototype.makeRequest = function (method, resource, body) {
        if (body === void 0) { body = null; }
        return this.http.request(resource, {
            method: method,
            body: body === undefined || body === null ? "" : body,
            responseType: http_1.ResponseContentType.Json
        })
            .map(function (res) { return res.json(); })
            .catch(function (error, caught) {
            if (error.status == 401)
                console.log('redirect');
            return Observable_1.Observable.throw(error.status + " - " + error.statusText);
        })
            .toPromise();
    };
    HttpClient = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, http_1.RequestOptions])
    ], HttpClient);
    return HttpClient;
}());
exports.HttpClient = HttpClient;
//# sourceMappingURL=httpclient.js.map