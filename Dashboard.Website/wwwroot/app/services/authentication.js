"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var app_config_1 = require('../app.config');
var token_key = "api_access_token";
var AuthToken = (function () {
    function AuthToken() {
    }
    AuthToken.prototype.toString = function () { return localStorage.getItem(token_key); };
    return AuthToken;
}());
exports.AuthToken = AuthToken;
var Authentication = (function () {
    function Authentication(http) {
        this.http = http;
        console.log(http);
    }
    Authentication.prototype.login = function (username, password) {
        var header = new http_1.Headers();
        header.append("Content-Type", "application/x-www-form-urlencoded");
        return this.http.post(app_config_1.AppConfig.baseUrl + "/auth/token", "grant_type=password&username=" + username + "&password=" + password, {
            headers: header
        })
            .map(function (res) { return res.json(); })
            .map(function (data) {
            localStorage.setItem(token_key, data.access_token);
            return true;
        }).toPromise();
    };
    Authentication = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], Authentication);
    return Authentication;
}());
exports.Authentication = Authentication;
//# sourceMappingURL=authentication.js.map