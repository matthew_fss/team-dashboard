"use strict";
var router_1 = require('@angular/router');
var login_1 = require('./pages/login');
var project_list_1 = require('./pages/project.list');
exports.routes = [
    { path: 'login', component: login_1.LoginComponent },
    { path: 'projects', component: project_list_1.ProjectListComponent },
    { path: '**', redirectTo: 'login' }
];
exports.routingProviders = [];
exports.router = router_1.RouterModule.forRoot(exports.routes);
//# sourceMappingURL=app.routes.js.map