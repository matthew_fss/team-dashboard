"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var httpclient_1 = require('../http/httpclient');
var ProjectListComponent = (function () {
    function ProjectListComponent(http) {
        var _this = this;
        this._projects = [];
        http.get('project').then(function (data) { return _this._projects = data; });
    }
    Object.defineProperty(ProjectListComponent.prototype, "projects", {
        get: function () { return this._projects; },
        enumerable: true,
        configurable: true
    });
    ProjectListComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/pages/project.list.html',
            styleUrls: ['app/pages/project.css']
        }), 
        __metadata('design:paramtypes', [httpclient_1.HttpClient])
    ], ProjectListComponent);
    return ProjectListComponent;
}());
exports.ProjectListComponent = ProjectListComponent;
//# sourceMappingURL=project.list.js.map