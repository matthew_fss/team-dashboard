"use strict";
var login_1 = require('./pages/login');
exports.routes = [
    { path: 'login', component: login_1.LoginComponent },
    { path: '**', redirectTo: 'login' }
];
