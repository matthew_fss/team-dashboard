"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var FulcrumLogo = (function () {
    function FulcrumLogo() {
    }
    FulcrumLogo = __decorate([
        core_1.Component({
            selector: 'fulcrum-logo',
            template: "\n        <div id=\"f\">F</div>\n        <div id=\"rest\">\n            <div id=\"ulcrum\">ulcrum</div>\n            <div id=\"dashboard\">Dashboard</div>\n        </div>\n    ",
            styles: [
                "\n        :host { \n            display: flex; \n            position: relative; \n            justify-content: center;\n            font-weight: bold;\n        }\n        #f { font-size: 3em; }\n        #ulcrum { \n            font-size: 1.25em;\n            transform: translateY(.5em);\n            text-decoration: underline;\n        }\n \n        #dashboard {\n            font-size: .7em;\n            font-style: italic;\n            transform: translateY(.5em);\n        }\n        "
            ],
            encapsulation: core_1.ViewEncapsulation.Native
        }), 
        __metadata('design:paramtypes', [])
    ], FulcrumLogo);
    return FulcrumLogo;
}());
exports.FulcrumLogo = FulcrumLogo;
