"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var authentication_1 = require('../services/authentication');
var placeholder_1 = require('../inputs/placeholder');
var logo_1 = require('../logo');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/delay');
var LoginComponent = (function () {
    function LoginComponent(auth) {
        this.auth = auth;
        this.authenticating = false;
        console.log(auth);
    }
    LoginComponent.prototype.signin = function (username, password) {
        var _this = this;
        this.authenticating = true;
        this.auth.login(username, password)
            .delay(4000)
            .subscribe(function (valid) { return console.log('Valid'); }, function (error) { return console.log('Error'); })
            .add(function () {
            _this.authenticating = false;
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: 'app/login/login.html',
            styleUrls: ['app/login/login.css'],
            providers: [
                core_1.provide(http_1.RequestOptions, { useValue: new http_1.RequestOptions() }),
                authentication_1.Authentication
            ],
            directives: [forms_1.FORM_DIRECTIVES, placeholder_1.InputPlaceholder, logo_1.FulcrumLogo],
            encapsulation: core_1.ViewEncapsulation.None
        }), 
        __metadata('design:paramtypes', [authentication_1.Authentication])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
