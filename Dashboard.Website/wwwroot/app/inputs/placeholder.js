"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var InputPlaceholder = (function () {
    function InputPlaceholder(element, loader, view) {
        this.element = element;
        this.loader = loader;
        this.view = view;
        this.container = this.loader.loadNextToLocation(PlaceholderContainer, this.view, core_1.ReflectiveInjector.resolve([core_1.provide(InputPlaceholder, { useValue: this })]))
            .then(function (ref) { return ref.instance.update(); });
    }
    Object.defineProperty(InputPlaceholder.prototype, "input", {
        get: function () {
            return this.element.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    InputPlaceholder.prototype.updatePlaceholder = function () { };
    InputPlaceholder.prototype.ngOnChanges = function () {
        this.input.placeholder = this.placeholder;
    };
    __decorate([
        core_1.Input('placeholder'), 
        __metadata('design:type', Object)
    ], InputPlaceholder.prototype, "placeholder", void 0);
    __decorate([
        core_1.HostListener('input'),
        core_1.HostListener('window:resize'), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', []), 
        __metadata('design:returntype', void 0)
    ], InputPlaceholder.prototype, "updatePlaceholder", null);
    InputPlaceholder = __decorate([
        core_1.Directive({
            selector: 'input[type=text][placeholder], input[type=password][placeholder]'
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.DynamicComponentLoader, core_1.ViewContainerRef])
    ], InputPlaceholder);
    return InputPlaceholder;
}());
exports.InputPlaceholder = InputPlaceholder;
var PlaceholderContainer = (function () {
    function PlaceholderContainer(parent, element) {
        this.parent = parent;
        this.element = element;
        this.height = 0;
        this.width = 0;
        this.zIndex = 0;
        this.offset = 0;
    }
    PlaceholderContainer.prototype.update = function () {
        var span = document.createElement('span');
        span.textContent = this.parent.placeholder;
        span.style.visibility = 'hidden';
        this.element.nativeElement.parentNode.insertBefore(span, this.element.nativeElement.nextElementSibling);
        this.height = span.offsetHeight;
        this.width = span.offsetWidth;
        this.offset = 5;
        this.element.nativeElement.parentNode.removeChild(span);
        this.zIndex = this.calculateZIndex(this.parent.input) + 1;
    };
    PlaceholderContainer.prototype.calculateZIndex = function (element) {
        if (!element)
            return 0;
        var z = parseInt(document.defaultView.getComputedStyle(element).getPropertyValue('z-index'));
        if (isNaN(z))
            return this.calculateZIndex(element.parentElement);
        return z;
    };
    PlaceholderContainer = __decorate([
        core_1.Component({
            selector: 'placeholder-container',
            template: "\n        <span *ngIf=\"!!parent.input.value.length\"\n            [@animation]=\"'fadeIn'\"\n            [style.top.px]=\"parent.input.offsetTop + (parent.input.offsetHeight - height) / 2\"\n            [style.left.px]=\"parent.input.offsetLeft + parent.input.offsetWidth - width - offset\"\n            [style.zIndex]=\"zIndex\"\n        >\n            {{parent.placeholder}}\n        </span>\n    ",
            styles: ["span { position: absolute; }"],
            animations: [
                core_1.trigger('animation', [
                    core_1.state('fadeIn', core_1.style({ opacity: 1 })),
                    core_1.transition('void => fadeIn', [
                        core_1.style({ opacity: 0 }),
                        core_1.animate(100)
                    ]),
                    core_1.transition('fadeIn => void', [
                        core_1.style({ opacity: 1 }),
                        core_1.animate(100, core_1.style({ opacity: 0 }))
                    ])
                ])
            ]
        }), 
        __metadata('design:paramtypes', [InputPlaceholder, core_1.ElementRef])
    ], PlaceholderContainer);
    return PlaceholderContainer;
}());
//# sourceMappingURL=placeholder.js.map