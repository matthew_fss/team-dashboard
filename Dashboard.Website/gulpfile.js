/// <binding Clean='clean' />
var ts = require('gulp-typescript');
var gulp = require('gulp');
var clean = require('gulp-clean');
var sass = require('gulp-sass');

var destPath = './wwwroot/libs/';

// Delete the dist directory
gulp.task('clean', function () {
    return gulp.src(destPath)
        .pipe(clean());
});

gulp.task("migrate", () => {
    gulp.src([
            'core-js/client/**',
            'systemjs/dist/system.src.js',
            'reflect-metadata/**',
            'rxjs/**',
            'zone.js/dist/**',
            '@angular/**',
            'jquery/dist/jquery.*js',
            'bootstrap/dist/js/bootstrap.*js',
    ], {
        cwd: "node_modules/**"
    })
    .pipe(gulp.dest("./wwwroot/libs"));
});

gulp.task('html', () => {
    gulp.src(['app/**/*.html']).pipe(gulp.dest("./wwwroot/app"));
});

gulp.task('css', () => {
    gulp.src(['app/**/*.css']).pipe(gulp.dest("./wwwroot/app"))
    gulp.src(['app/**/*.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./wwwroot/app'));
});

var tsProject = ts.createProject('app/tsconfig.json');
gulp.task('ts', function (done) {
    //var tsResult = tsProject.src()
    var tsResult = gulp.src([
            "app/**/*.ts"
    ])
        .pipe(ts(tsProject), undefined, ts.reporter.fullReporter());
    return tsResult.js.pipe(gulp.dest('./wwwroot/app'));
});

gulp.task('watch', ['watch.ts', 'watch.html', 'watch.css']);

gulp.task('watch.ts', ['ts'], function () {
    return gulp.watch('app/**/*.ts', ['ts']);
});

gulp.task('watch.html', ['html'], function () {
    return gulp.watch('app/**/*.html', ['html']);
});

gulp.task('watch.css', ['css'], function () {
    return gulp.watch(['app/**/*.css', 'app/**/*.scss'], ['css']);
});

gulp.task('default', ['migrate', 'watch']);