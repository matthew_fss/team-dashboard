﻿using System;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.DependencyInjection
{
    public static class DependencyInjectorExtensions
    {
        public static IDependencyInjector Register(this IDependencyInjector injector, Registration registration)
        {
            registration.PerformRegistration(injector);
            return injector;
        }

        #region Scoped
        public static IDependencyInjector ScopedType(this IDependencyInjector injector, Type from)
            => Execute(injector, () => injector.RegisterType(from, LifeCycle.Scoped));

        public static IDependencyInjector ScopedType<TFrom, TTo>(this IDependencyInjector injector) where TTo : TFrom
            => Execute(injector, () => injector.RegisterType<TFrom, TTo>(LifeCycle.Scoped));

        public static IDependencyInjector ScopedType(this IDependencyInjector injector, Type from, Type to)
            => Execute(injector, () => injector.RegisterType(from, to, LifeCycle.Scoped));

        public static IDependencyInjector ScopedInstance<T>(this IDependencyInjector injector, T instance)
            => Execute(injector, () => injector.RegisterInstance(instance, LifeCycle.Scoped));

        public static IDependencyInjector ScopedFactory(this IDependencyInjector injector, Type type, Func<IDependencyInjector, object> factory)
            => Execute(injector, () => injector.RegisterFactory(type, factory));

        public static IDependencyInjector ScopedFactory(this IDependencyInjector injector, Type type, Func<IDependencyInjector, Type, object> factory)
           => Execute(injector, () => injector.RegisterFactory(type, factory));

        public static IDependencyInjector ScopedFactory<T>(this IDependencyInjector injector, Func<IDependencyInjector, T> factory)
           => Execute(injector, () => injector.RegisterFactory<T>(factory));

        public static IDependencyInjector ScopedFactory<T>(this IDependencyInjector injector, Func<IDependencyInjector, Type, T> factory)
           => Execute(injector, () => injector.RegisterFactory<T>(factory));

        #endregion

        #region Transient
        public static IDependencyInjector TransientType(this IDependencyInjector injector, Type from)
            => Execute(injector, () => injector.RegisterType(from, LifeCycle.Transient));

        public static IDependencyInjector TransientType<TFrom, TTo>(this IDependencyInjector injector) where TTo : TFrom
            => Execute(injector, () => injector.RegisterType<TFrom, TTo>(LifeCycle.Transient));

        public static IDependencyInjector TransientType(this IDependencyInjector injector, Type from, Type to)
            => Execute(injector, () => injector.RegisterType(from, to, LifeCycle.Transient));

        public static IDependencyInjector TransientInstance<T>(this IDependencyInjector injector, T instance)
            => Execute(injector, () => injector.RegisterInstance(instance, LifeCycle.Transient));

        public static IDependencyInjector TransientFactory(this IDependencyInjector injector, Type type, Func<IDependencyInjector, object> factory)
            => Execute(injector, () => injector.RegisterFactory(type, factory, LifeCycle.Transient));

        public static IDependencyInjector TransientFactory(this IDependencyInjector injector, Type type, Func<IDependencyInjector, Type, object> factory)
           => Execute(injector, () => injector.RegisterFactory(type, factory, LifeCycle.Transient));

        public static IDependencyInjector TransientFactory<T>(this IDependencyInjector injector, Func<IDependencyInjector, T> factory)
           => Execute(injector, () => injector.RegisterFactory<T>(factory, LifeCycle.Transient));

        public static IDependencyInjector TransientFactory<T>(this IDependencyInjector injector, Func<IDependencyInjector, Type, T> factory)
           => Execute(injector, () => injector.RegisterFactory<T>(factory, LifeCycle.Transient));

        #endregion

        #region Singleton
        public static IDependencyInjector SingletonType(this IDependencyInjector injector, Type from)
           => Execute(injector, () => injector.RegisterType(from, LifeCycle.Singleton));

        public static IDependencyInjector SingletonType<TFrom, TTo>(this IDependencyInjector injector) where TTo : TFrom
            => Execute(injector, () => injector.RegisterType<TFrom, TTo>(LifeCycle.Singleton));

        public static IDependencyInjector SingletonType(this IDependencyInjector injector, Type from, Type to)
            => Execute(injector, () => injector.RegisterType(from, to, LifeCycle.Singleton));

        public static IDependencyInjector SingletonInstance<T>(this IDependencyInjector injector, T instance)
            => Execute(injector, () => injector.RegisterInstance(instance, LifeCycle.Singleton));

        public static IDependencyInjector SingletonFactory(this IDependencyInjector injector, Type type, Func<IDependencyInjector, object> factory)
            => Execute(injector, () => injector.RegisterFactory(type, factory, LifeCycle.Singleton));

        public static IDependencyInjector SingletonFactory(this IDependencyInjector injector, Type type, Func<IDependencyInjector, Type, object> factory)
           => Execute(injector, () => injector.RegisterFactory(type, factory, LifeCycle.Singleton));

        public static IDependencyInjector SingletonFactory<T>(this IDependencyInjector injector, Func<IDependencyInjector, T> factory)
           => Execute(injector, () => injector.RegisterFactory<T>(factory, LifeCycle.Singleton));

        public static IDependencyInjector SingletonFactory<T>(this IDependencyInjector injector, Func<IDependencyInjector, Type, T> factory)
           => Execute(injector, () => injector.RegisterFactory<T>(factory, LifeCycle.Singleton));

        #endregion

        private static IDependencyInjector Execute(IDependencyInjector injector, Action action)
        {
            action();
            return injector;
        }
    }
}
