﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dashboard.Infrastructure.DependencyInjection
{
    public partial class Registration
    {
        private IEnumerable<Type> register;

        public Registration(Type type) : this(new [] { type }) { }
        
        public Registration(IEnumerable<Type> types)
        {
            register = types;
        }

        private Func<Type, IEnumerable<Type>> fortype_mapping;
        public Registration ForType(Type type)
            => ForTypes(t => new[] { type });

        public Registration ForTypes(IEnumerable<Type> types)
            => ForTypes(t => types);

        public Registration ForTypes(Func<Type, IEnumerable<Type>> map)
        {
            fortype_mapping = map;
            return this;
        }

        private Func<Type, LifeCycle> lifecyleMapping;
        public Registration WithLifeCycle(LifeCycle lifecycle)
            => WithLifeCycle(t => lifecycle);

        public Registration WithLifeCycle(Func<Type, LifeCycle> mapping)
        {
            lifecyleMapping = mapping;
            return this;
        }

        internal void PerformRegistration(IDependencyInjector injector)
        {
            foreach(Type to in register)
            {
                if (fortype_mapping == null)
                    BasicRegistration(injector, to);
                else
                    ForeachRegistration(injector, to, fortype_mapping(to));
            }
        }

        private void BasicRegistration(IDependencyInjector injector, Type t)
        {
            LifeCycle cycle = GetLifeCycle(t);

            injector.RegisterType(t, cycle);
        }

        private void ForeachRegistration(IDependencyInjector injector, Type to, IEnumerable<Type> from_list)
        {
            LifeCycle cycle = GetLifeCycle(to);
            foreach (Type from in from_list)
            {
                injector.RegisterType(from, to, cycle);
            }
        }

        private LifeCycle GetLifeCycle(Type t)
        {
            return lifecyleMapping?.Invoke(t) ?? LifeCycle.Transient;
        }
    }
}
