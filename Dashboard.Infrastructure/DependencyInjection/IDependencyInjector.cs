﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace Dashboard.Infrastructure.DependencyInjection
{
    public interface IDependencyInjector : IServiceProvider
    {
        void RegisterType(Type from, LifeCycle lifecycle = LifeCycle.Transient);

        void RegisterType(Type from, Type to, LifeCycle lifecycle = LifeCycle.Transient);

        void RegisterType<TFrom, TTo>(LifeCycle lifecycle = LifeCycle.Transient)
            where TTo : TFrom;

        void RegisterInstance<T>(T instance, LifeCycle lifecycle = LifeCycle.Transient);

        void RegisterFactory(Type type, Func<IDependencyInjector, object> factory, LifeCycle lifecycle = LifeCycle.Transient);

        void RegisterFactory(Type type, Func<IDependencyInjector, Type, object> factory, LifeCycle lifecycle = LifeCycle.Transient);

        void RegisterFactory<T>(Func<IDependencyInjector, T> factory, LifeCycle lifecycle = LifeCycle.Transient);

        void RegisterFactory<T>(Func<IDependencyInjector, Type, T> factory, LifeCycle lifecycle = LifeCycle.Transient);

        object Resolve(Type type);

        T Resolve<T>();

        IDependencyInjector CreateChildInjector();
    }
}
