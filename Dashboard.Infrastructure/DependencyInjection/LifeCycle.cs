﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.DependencyInjection
{
    public enum LifeCycle
    {
        Scoped,
        Transient,
        Singleton
    }
}
