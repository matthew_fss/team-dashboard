﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace Dashboard.Infrastructure.DependencyInjection
{
    public class UnityDependencyInjector : IDependencyInjector, IDependencyResolver, IDependencyScope
    {
        private IUnityContainer _container;
        public UnityDependencyInjector() : this(new UnityContainer()) { }

        private UnityDependencyInjector(IUnityContainer container)
        {
            _container = container;
        }

        public IUnityContainer UnityContainer => _container;

        #region IServiceProvider
        object IServiceProvider.GetService(Type serviceType) => Resolve(serviceType);
        #endregion

        #region IDependencyInjector
        public void RegisterType(Type from, LifeCycle lifecycle = LifeCycle.Transient) 
            => _container.RegisterType(from, CreateLifeTimeManager(lifecycle));

        public void RegisterType(Type from, Type to, LifeCycle lifecycle = LifeCycle.Transient) 
            => _container.RegisterType(from, to, CreateLifeTimeManager(lifecycle));

        public void RegisterType<TFrom, TTo>(LifeCycle lifecycle = LifeCycle.Transient) where TTo : TFrom
            => _container.RegisterType<TFrom, TTo>(CreateLifeTimeManager(lifecycle));

        public void RegisterInstance<T>(T instance, LifeCycle lifecycle = LifeCycle.Transient)
            => _container.RegisterInstance<T>(instance, CreateLifeTimeManager(lifecycle));
        
        public void RegisterFactory(Type type, Func<IDependencyInjector, object> factory, LifeCycle lifecycle = LifeCycle.Transient)
            => _container.RegisterType(type, CreateLifeTimeManager(lifecycle), new InjectionFactory(c => factory(this)));

        public void RegisterFactory(Type type, Func<IDependencyInjector, Type, object> factory, LifeCycle lifecycle = LifeCycle.Transient)
            => _container.RegisterType(type, CreateLifeTimeManager(lifecycle), new InjectionFactory((c, t, n) => factory(this, t)));

        public void RegisterFactory<T>(Func<IDependencyInjector, T> factory, LifeCycle lifecycle = LifeCycle.Transient)
            => _container.RegisterType<T>(CreateLifeTimeManager(lifecycle), new InjectionFactory(c => factory(this)));

        public void RegisterFactory<T>(Func<IDependencyInjector, Type, T> factory, LifeCycle lifecycle = LifeCycle.Transient)
            => _container.RegisterType<T>(CreateLifeTimeManager(lifecycle), new InjectionFactory((c, t, n) => factory(this, t)));

        public object Resolve(Type type)
        {
            try
            {
                return _container.Resolve(type);
            }
            catch (ResolutionFailedException ex)
            {
                System.Diagnostics.Debug.WriteLine($"Exception: {ex.Message}");
                System.Diagnostics.Debug.WriteLine($"Stack Trace: {ex.StackTrace}");

                return null;
            }
        }

        public T Resolve<T>()
        {
            try
            {
                return _container.Resolve<T>();
            }
            catch (ResolutionFailedException ex)
            {
                System.Diagnostics.Debug.WriteLine($"Exception: {ex.Message}");
                System.Diagnostics.Debug.WriteLine($"Stack Trace: {ex.StackTrace}");

                return default(T);
            }
        }

        public IDependencyInjector CreateChildInjector() => new UnityDependencyInjector(_container.CreateChildContainer());
        #endregion

        private LifetimeManager CreateLifeTimeManager(LifeCycle lifecycle)
        {
            switch(lifecycle)
            {
                case LifeCycle.Scoped:
                    return new HierarchicalLifetimeManager();
                case LifeCycle.Singleton:
                    return new ContainerControlledLifetimeManager();
                case LifeCycle.Transient:
                    return new TransientLifetimeManager();
                default:
                    throw new NotSupportedException($"{lifecycle} life cycle is not supported");
            }
        }

        #region IDependencyResolver
        IDependencyScope IDependencyResolver.BeginScope() => CreateChildInjector() as UnityDependencyInjector;
        #endregion

        #region IDependencyScope
        object IDependencyScope.GetService(Type serviceType) => Resolve(serviceType);

        IEnumerable<object> IDependencyScope.GetServices(Type serviceType) => _container.ResolveAll(serviceType);
        #endregion

        #region IDisposable
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                    _container.Dispose();

                disposedValue = true;
            }
        }
        
        void IDisposable.Dispose() => Dispose(true);

        #endregion
    }
}
