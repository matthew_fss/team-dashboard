﻿using Dashboard.Infrastructure.Typing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.DependencyInjection
{
    public partial class Registration
    {
        public static Registration ForOpenGeneric(Type openGeneric, Assembly[] fromAssemblies = null)
            => new Registration(Classes.OfType(openGeneric, fromAssemblies)).ForTypes(Mapping.Interfaces);
        
		public static class Classes
        {
            public static IEnumerable<Type> OfType<T>(IEnumerable<Assembly> fromAssemblies = null) where T : class
                => OfType(typeof(T));

            public static IEnumerable<Type> OfType(Type type, IEnumerable<Assembly> fromAssemblies = null)
            {
				return from assembly in (fromAssemblies ?? new[] { type.Assembly }).Distinct()
					where !assembly.IsDynamic
					from t in assembly.GetTypes()
					where t.IsConcrete()
					where type.IsAssignableFromImplementation(t)
					select t;
            }
        }

		public static class Mapping
        {
			public static IEnumerable<Type> Interfaces(Type type)
                => type.GetInterfaces();
        }
    }
}
