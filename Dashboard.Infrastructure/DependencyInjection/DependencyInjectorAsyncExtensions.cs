﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.DependencyInjection
{
    public static class DependencyInjectorAsyncExtensions
    {
        public static Task<object> ResolveAsync(this IDependencyInjector injector, Type type)
        {
            var task = Task.Factory.StartNew(() => injector.Resolve(type));
            task.ConfigureAwait(false);
            return task;
        }

        public static Task<T> ResolveAsync<T>(this IDependencyInjector injector)
        {
            var task = Task.Factory.StartNew(() => injector.Resolve<T>());
            task.ConfigureAwait(false);
            return task;
        }
    }
}
