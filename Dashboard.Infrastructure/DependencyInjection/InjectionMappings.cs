﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.DependencyInjection
{
    public static class InjectionMappings
    {
        public static Func<IDependencyInjector, Type, object> FromAssemblies(params Assembly[] assemblies)
        {
            return (injector, type) =>
            {
                if (IsConcrete(type))
                    return injector.Resolve(type);

                return null;
            };
        }

        private static bool IsConcrete(Type t)
        {
            return t.IsClass && !t.IsAbstract;
        }
    }
}
