﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Jobs
{
    interface IJob
    {
        Task ExecuteAsync();
    }

    public interface IJob<T>
    {
        Task<T> ExecuteAsnyc();
    }
}
