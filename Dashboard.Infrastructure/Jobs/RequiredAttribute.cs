﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Jobs
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Parameter)]
    public class RequiredAttribute : Attribute
    {
        private Type[] _tasks;
        public RequiredAttribute(params Type[] tasks)
        {
            _tasks = tasks;
        }
    }
}
