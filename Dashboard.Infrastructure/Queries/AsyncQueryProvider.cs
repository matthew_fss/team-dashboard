﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Queries
{
    public abstract class AsyncQueryProvider : QueryProvider, IDbAsyncQueryProvider
    {
        public override object Execute(Expression expression) => ExecuteAsync(expression, default(CancellationToken)).Result;

        public abstract Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken);

        public async Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
            => (TResult)(await ExecuteAsync(expression, default(CancellationToken)));
    }
}
