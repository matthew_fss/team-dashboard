﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Queries
{
    public interface IQueryHandler<in TIn, out TOut>
        where TIn : IQuery<TOut>
    {
        TOut Execute(TIn query);
    }
}
