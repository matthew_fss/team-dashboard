﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Queries
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private IServiceProvider _provider;
        public QueryDispatcher(IServiceProvider provider)
        {
            _provider = provider;
        }

        public virtual TResult Dispatch<TResult>(IQuery<TResult> query)
        {
            dynamic handler = _provider.GetService(typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult)));

            return handler != null ? handler.Execute((dynamic)query) : default(TResult);
        }

        public virtual async Task<TResult> DispatchAsync<TResult>(IAsyncQuery<TResult> query)
        {
            dynamic handler = _provider.GetService(typeof(IAsyncQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult)));

            return handler != null ? await handler.ExecuteAsync((dynamic)query) : default(TResult);
        }
    }
}
