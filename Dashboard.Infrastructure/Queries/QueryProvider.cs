﻿using Dashboard.Infrastructure.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Queries
{
    public abstract class QueryProvider : IQueryProvider
    {
        protected QueryProvider() { }

        public abstract string GetQueryText(Expression expression);

        public abstract object Execute(Expression expression);

        public virtual IQueryable<TEntity> CreateQuery<TEntity>(Expression expression)
            => new Queryable<TEntity>(this, expression);
        
        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            Type elementType = TypeHelper.GetElementType(expression.Type);

            try
            {
                return (IQueryable)Activator.CreateInstance(typeof(Queryable<>).MakeGenericType(elementType), new object[] { this, expression });
            }
            catch (TargetInvocationException tie)
            {
                throw tie.InnerException;
            }
        }

        TEntity IQueryProvider.Execute<TEntity>(Expression expression)
            => (TEntity)Execute(expression);
        
        object IQueryProvider.Execute(Expression expression)
            => Execute(expression);
    }
}
