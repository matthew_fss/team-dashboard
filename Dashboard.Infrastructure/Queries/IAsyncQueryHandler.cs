﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Queries
{
    public interface IAsyncQueryHandler<in TIn, TOut>
        where TIn : IAsyncQuery<TOut>
    {
        Task<TOut> ExecuteAsync(TIn query);
    }
}
