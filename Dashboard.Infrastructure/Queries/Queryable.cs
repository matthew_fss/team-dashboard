﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Queries
{
    public class Queryable<T> : IQueryable<T>, IQueryable, IEnumerable<T>, IEnumerable, IOrderedQueryable<T>, IOrderedQueryable
    {
        private IQueryProvider _provider;
        private Expression _expression;
        
        public Queryable(IQueryProvider provider)
        {
            if (provider == null)
                throw new ArgumentNullException(nameof(provider));

            _provider = provider;
            _expression = Expression.Constant(this);
        }

        public Queryable(IQueryProvider provider, Expression expression)
        {
            if (provider == null)
                throw new ArgumentNullException(nameof(provider));

            if (expression == null)
                throw new ArgumentNullException(nameof(expression));

            if (!typeof(IQueryable<T>).IsAssignableFrom(expression.Type))
                throw new ArgumentOutOfRangeException(nameof(expression));

            _provider = provider;
            _expression = expression;
        }
        
        Expression IQueryable.Expression => _expression;
        
        Type IQueryable.ElementType => typeof(T);

        IQueryProvider IQueryable.Provider => _provider;

        public IEnumerator<T> GetEnumerator() 
            => ((IEnumerable<T>)_provider.Execute(_expression)).GetEnumerator();
        
        IEnumerator IEnumerable.GetEnumerator()
            => ((IEnumerable)_provider.Execute(_expression)).GetEnumerator();
        
        public override string ToString()
            => _provider is QueryProvider
                ? ((QueryProvider)_provider).GetQueryText(_expression)
                : _expression.ToString();
    }
}
