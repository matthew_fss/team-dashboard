﻿using Dashboard.Infrastructure.Expressions;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;

namespace Dashboard.Infrastructure.Queries
{
    public class AsyncEnumerableQuery<T> : IQueryable, IQueryable<T>, IQueryProvider, IDbAsyncQueryProvider
    {
        private EnumerableQuery<T> query;
        public AsyncEnumerableQuery(IEnumerable<T> list) : this(() => list) { }

        public AsyncEnumerableQuery(Func<IEnumerable<T>> factory) : this(
            Expression.Constant(
                new EnumerableQuery<T>(
                    Expression.Call(
                        Expression.Constant(factory.Target),
                        factory.Method
                    )
                )
            ))
        { }

        internal AsyncEnumerableQuery(Expression exp) { query = new EnumerableQuery<T>(exp); }

        public Expression Expression => ((IQueryable)query).Expression;

        public Type ElementType => ((IQueryable)query).ElementType;

        public IQueryProvider Provider => this;

        public Task<object> ExecuteAsync(Expression expression, CancellationToken cancellationToken)
            => Task.Factory.StartNew(() => Execute(expression));

        public Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
            => Task.Factory.StartNew(() => Execute<TResult>(expression));

        public IEnumerator GetEnumerator() => ((IEnumerable)query).GetEnumerator();

        IEnumerator<T> IEnumerable<T>.GetEnumerator() => ((IEnumerable<T>)query).GetEnumerator();

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression) => new AsyncEnumerableQuery<TElement>(expression);

        public object Execute(Expression expression) => ((IQueryProvider)query).Execute(expression);

        public TResult Execute<TResult>(Expression expression) => ((IQueryProvider)query).Execute<TResult>(expression);

        public IQueryable CreateQuery(Expression expression) => ((IQueryProvider)query).CreateQuery(expression);
    }
}
