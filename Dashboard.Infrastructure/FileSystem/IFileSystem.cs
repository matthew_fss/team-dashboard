﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.FileSystem
{
    public interface IFileSystem
    {
        string CombinePath(params string[] paths);

        bool FileExists(string path);

        byte[] ReadFile(string path);
    }
}
