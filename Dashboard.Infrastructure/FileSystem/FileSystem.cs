﻿using System.IO;
using static System.IO.Path;
using static System.IO.File;

namespace Dashboard.Infrastructure.FileSystem
{
    public class FileSystem : IFileSystem
    {
        public string CombinePath(params string[] paths) 
            => Combine(paths);

        public bool FileExists(string path)
            => new FileInfo(path).Exists;

        public byte[] ReadFile(string path)
            => ReadAllBytes(path);
    }
}
