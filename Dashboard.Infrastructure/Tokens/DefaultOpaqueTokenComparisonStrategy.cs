﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Tokens
{
    public class DefaultOpaqueTokenComparisonStrategy<TKey> : IOpaqueTokenComparisonStrategy<TKey>
    {
        private EqualityComparer<TKey> @default = EqualityComparer<TKey>.Default;

        public int Compare(TKey a, TKey b)
        {
            throw new NotImplementedException();
        }

        public int HashCode(TKey obj) => @default.GetHashCode(obj);

        public bool Same(TKey a, TKey b) => @default.Equals(a, b);
    }
}
