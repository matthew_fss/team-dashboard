﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Tokens
{
    public interface IOpaqueTokenComparisonStrategy<TKey>
    {
        /// <summary>
        /// Tests whether 'a' and 'b' represents the same token.
        /// </summary>
        /// <param name="a">A token key</param>
        /// <param name="b"></param>
        /// <returns>
        /// True if and only if 'a' represents the same token as 'b'; i.e. a and b are equal
        /// </returns>
        bool Same(TKey a, TKey b);

        int Compare(TKey a, TKey b);

        /// <summary>
        /// Produces a hash code for the object that can be used 
        /// in dictionaries.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>
        /// An integer hash code for an object.
        /// </returns>
        /// <remarks>
        /// If the value of Same(a, b) for keys 'a' and 'b' returns true,
        /// then the HashCode(a) must equal HashCode(b)
        /// </remarks>
        int HashCode(TKey obj);
    }
}
