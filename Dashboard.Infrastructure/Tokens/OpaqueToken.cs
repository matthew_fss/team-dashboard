﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Tokens
{
    public class OpaqueToken<TKey>
    {
        protected TKey Key { get; private set; }

        public OpaqueToken(TKey key) : this(key, new DefaultOpaqueTokenComparisonStrategy<TKey>()) { }
        protected OpaqueToken(TKey key, IOpaqueTokenComparisonStrategy<TKey> comparison)
        {
            Key = key;
            ComparisonStrategy = comparison;
        }

        protected IOpaqueTokenComparisonStrategy<TKey> ComparisonStrategy { get; private set; } 
        
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            OpaqueToken<TKey> token = obj as OpaqueToken<TKey>;

            if ((object)token == null)
                return false;
            
            return ComparisonStrategy.Same(Key, token.Key);
        }

        public override int GetHashCode() => ComparisonStrategy.HashCode(Key);

        public static bool operator ==(OpaqueToken<TKey> a, OpaqueToken<TKey> b)
        {
            if (ReferenceEquals(a, b))
                return true;

            if ((object)a == null || (object)b == null)
                return false;

            return a.GetType() == b.GetType() && a.ComparisonStrategy.Same(a.Key, b.Key);
        }

        public static bool operator !=(OpaqueToken<TKey> a, OpaqueToken<TKey> b) => !(a == b);
    }
}
