﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Tokens
{
    public class StringToken : OpaqueToken<string>
    {
        public StringToken(string value, bool ignoreCase = false)
            : base(value, new StringTokenComparisonStrategy(ignoreCase)) { }
        
        public override string ToString() => Key;
        
        public static bool operator >(StringToken left, StringToken right)
            => left.ComparisonStrategy.Compare(left.Key, right.Key) > 0;
        public static bool operator >=(StringToken left, StringToken right)
            => left.ComparisonStrategy.Compare(left.Key, right.Key) >= 0; 
        public static bool operator <(StringToken left, StringToken right) => !(left >= right);
        public static bool operator <=(StringToken left, StringToken right) => !(left > right);


        public static implicit operator string(StringToken token) => token.Key;
        public static implicit operator StringToken(string value) => new StringToken(value);
    }

    public class StringTokenComparisonStrategy : IOpaqueTokenComparisonStrategy<string>
    {
        private StringComparer comparer;
        public StringTokenComparisonStrategy(bool ignoreCase)
        {
            comparer = ignoreCase
                ? StringComparer.OrdinalIgnoreCase
                : StringComparer.Ordinal;
        }

        public int Compare(string a, string b) => comparer.Compare(a, b);

        public int HashCode(string obj) => comparer.GetHashCode(obj);

        public bool Same(string a, string b) => comparer.Equals(a, b);
    }
}
