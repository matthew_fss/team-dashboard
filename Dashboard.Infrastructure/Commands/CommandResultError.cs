﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Commands
{
    public class CommandResultError : IDictionary<string, IEnumerable<string>>
    {
        private Dictionary<string, IEnumerable<string>> errors = new Dictionary<string, IEnumerable<string>>();
        IEnumerable<string> IDictionary<string, IEnumerable<string>>.this[string key]
        {
            get { return errors[key]; }
            set { errors[key] = value; }
        }

        public int Count => errors.Aggregate(0, (total, next) => total + next.Value.Count());

        bool ICollection<KeyValuePair<string, IEnumerable<string>>>.IsReadOnly { get; } = false;

        ICollection<string> IDictionary<string, IEnumerable<string>>.Keys => errors.Keys;

        ICollection<IEnumerable<string>> IDictionary<string, IEnumerable<string>>.Values => errors.Values;

        void ICollection<KeyValuePair<string, IEnumerable<string>>>.Add(KeyValuePair<string, IEnumerable<string>> item)
            => ((ICollection<KeyValuePair<string, IEnumerable<string>>>)errors).Add(item);

        void IDictionary<string, IEnumerable<string>>.Add(string key, IEnumerable<string> value)
            => errors.Add(key, value);

        public CommandResultError Add(params string[] errorMessages) => Add(null, errorMessages);

        public CommandResultError Add(string key, params string[] errorMessages)
        {
            key = key ?? string.Empty;

            IEnumerable<string> collection;
            if(!(errors.TryGetValue(key, out collection) && collection is List<string>))
            {
                collection = new List<string>();

                if (errors.ContainsKey(key))
                    errors[key] = collection;
                else
                    errors.Add(key, collection);
            }

            (collection as List<string>).AddRange(errorMessages);

            return this;
        }

        void ICollection<KeyValuePair<string, IEnumerable<string>>>.Clear()
            => errors.Clear();

        bool ICollection<KeyValuePair<string, IEnumerable<string>>>.Contains(KeyValuePair<string, IEnumerable<string>> item)
            => ((ICollection<KeyValuePair<string, IEnumerable<string>>>)errors).Contains(item);

        bool IDictionary<string, IEnumerable<string>>.ContainsKey(string key)
            => errors.ContainsKey(key);

        void ICollection<KeyValuePair<string, IEnumerable<string>>>.CopyTo(KeyValuePair<string, IEnumerable<string>>[] array, int arrayIndex)
            => ((ICollection<KeyValuePair<string, IEnumerable<string>>>)errors).CopyTo(array, arrayIndex);

        IEnumerator IEnumerable.GetEnumerator()
            => errors.GetEnumerator();

        IEnumerator<KeyValuePair<string, IEnumerable<string>>> IEnumerable<KeyValuePair<string, IEnumerable<string>>>.GetEnumerator()
            => errors.GetEnumerator();

        bool ICollection<KeyValuePair<string, IEnumerable<string>>>.Remove(KeyValuePair<string, IEnumerable<string>> item)
            => ((ICollection<KeyValuePair<string, IEnumerable<string>>>)errors).Remove(item);

        bool IDictionary<string, IEnumerable<string>>.Remove(string key) => errors.Remove(key);

        bool IDictionary<string, IEnumerable<string>>.TryGetValue(string key, out IEnumerable<string> value)
            => errors.TryGetValue(key, out value);
    }
}
