﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Commands
{
    public class CommandDispatcher : ICommandDispatcher
    {
        private IServiceProvider _provider;
        public CommandDispatcher(IServiceProvider provider)
        {
            _provider = provider;
        }

        public CommandResult Dispatch(ICommand command)
        {
            throw new NotImplementedException();
        }

        public CommandResult<TResult> Dispatch<TResult>(ICommand<TResult> command) where TResult : struct
        {
            throw new NotImplementedException();
        }

        public Task<CommandResult> DispatchAsync(ICommand command)
        {
            throw new NotImplementedException();
        }

        public Task<CommandResult<TResult>> DispatchAsync<TResult>(ICommand<TResult> command) where TResult : struct
        {
            throw new NotImplementedException();
        }
    }
}
