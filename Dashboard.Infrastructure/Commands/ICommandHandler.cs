﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Commands
{
    public interface ICommandHandler<in TCommand>
    {
        void Execute(TCommand command);
    }
}
