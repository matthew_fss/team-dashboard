﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Commands
{
    public static class CommandResultExtensions
    {
        public static TCommandResult MakeOK<TCommandResult>(this TCommandResult result)
            where TCommandResult : CommandResult
        {
            result.Success = true;
            result.Errors = null;

            return result;
        }

        public static CommandResult<TValue> Value<TCommandResult, TValue>(this TCommandResult result, TValue value)
            where TCommandResult : CommandResult
            where TValue : struct
        {
            return new CommandResult<TValue>(value)
            {
                Success = result.Success,
                Errors = result.Errors
            };
        }

        public static TCommandResult FailMessage<TCommandResult>(this TCommandResult result, string errorMessage)
            where TCommandResult : CommandResult
            => FailMessages(result, errorMessage);

        public static TCommandResult FailMessages<TCommandResult>(this TCommandResult result, params string[] errorMessages)
            where TCommandResult : CommandResult
        {
            result.Errors.Add(errorMessages);
            return result;
        }

        public static TCommandResult FailMessage<TCommandResult>(this TCommandResult result, string key, string errorMessage)
            where TCommandResult : CommandResult
            => FailMessages(result, key, errorMessage);

        public static TCommandResult FailMessages<TCommandResult>(this TCommandResult result, string key, params string[] errorMessages)
            where TCommandResult : CommandResult
        {
            result.Errors.Add(key, errorMessages);
            return result;
        }
    }
}
