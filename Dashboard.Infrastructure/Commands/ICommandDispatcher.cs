﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Commands
{
    public interface ICommandDispatcher
    {
        CommandResult Dispatch(ICommand command);

        CommandResult<TResult> Dispatch<TResult>(ICommand<TResult> command) where TResult : struct;
        
        Task<CommandResult> DispatchAsync(ICommand command);

        Task<CommandResult<TResult>> DispatchAsync<TResult>(ICommand<TResult> command) where TResult: struct;
    }
}
