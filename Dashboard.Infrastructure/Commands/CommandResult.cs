﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Commands
{
    public class CommandResult
    {
        protected CommandResult() { }

        public bool Success { get; internal set; }
        
        public CommandResultError Errors { get; internal set; }

        public static CommandResult OK() => new CommandResult()
        {
            Success = true
        };

        public static CommandResult Fail() => new CommandResult()
        {
            Success = false
        };
    }

    public class CommandResult<TValue> : CommandResult
        where TValue : struct
    {
        internal CommandResult(TValue value) : base()
        {
            Value = value;
        }

        public TValue Value { get; }
    }
}
