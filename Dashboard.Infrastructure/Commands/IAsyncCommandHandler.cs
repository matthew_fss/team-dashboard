﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Commands
{
    public interface IAsyncCommandHandler<in TCommand>
        where TCommand : ICommand
    {
        Task<CommandResult> ExecuteAsync(TCommand command);
    }

    public interface IAsyncCommandHandler<in TCommand, TResult>
        where TCommand : ICommand<TResult>
        where TResult: struct
    {
        Task<CommandResult<TResult>> ExecuteAsync(TCommand command);
    }
}
