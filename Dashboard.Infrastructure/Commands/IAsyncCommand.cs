﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Commands
{
    public interface IAsyncCommand { }

    public interface IAsyncCommand<TResult> where TResult : struct { }
}
