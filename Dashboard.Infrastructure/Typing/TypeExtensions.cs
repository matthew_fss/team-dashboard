﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Typing
{
    public static class TypeExtensions
    {
        public static bool IsConcrete(this Type type)
            => !type.IsInterface && !type.IsAbstract;

        public static bool IsAssignableFromImplementation(this Type type, Type implementationType)
        {
            var x = implementationType.Name;

            if (!type.IsGenericType)
                return type.IsAssignableFrom(implementationType);

            if (implementationType.IsGenericType && implementationType.GetGenericTypeDefinition() == type)
                return true;

            foreach(Type interfaceType in implementationType.GetInterfaces())
            {
                if (IsGenericImplementationOf(interfaceType, type))
                {
                    return true;
                }
            }

            Type baseType = implementationType.BaseType ?? (implementationType != typeof(object) ? typeof(object) : null);

            while (baseType != null)
            {
                if (IsGenericImplementationOf(baseType, type))
                    return true;

                baseType = baseType.BaseType;
            }

            return false;
        }

        public static bool IsGenericImplementationOf(this Type implentationType, Type type)
            => implentationType == type
                || type.IsVariantVersionOf(implentationType)
                || (implentationType.IsGenericType && implentationType.GetGenericTypeDefinition() == type);

        public static bool IsVariantVersionOf(this Type type, Type otherType) =>
            type.IsGenericType
            && otherType.IsGenericType
            && type.GetGenericTypeDefinition() == otherType.GetGenericTypeDefinition()
            && type.IsAssignableFrom(otherType);
    }
}
