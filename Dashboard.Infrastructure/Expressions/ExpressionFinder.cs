﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Expressions
{
    public class ExpressionFinder : ExpressionVisitor
    {
        private Func<Expression, bool> _evaluator;
        private HashSet<Expression> found;
        private bool notEvaluatable;
        public ExpressionFinder(Func<Expression, bool> evaluator)
        {
            _evaluator = evaluator;
        }

        public HashSet<Expression> Find(Expression exp)
        {
            found = new HashSet<Expression>();
            Visit(exp);
            return found;
        }

        public override Expression Visit(Expression exp)
        {
            if(exp != null)
            {
                bool savedNotEvaluatable = notEvaluatable;
                notEvaluatable = false;
                base.Visit(exp);

                if(!notEvaluatable)
                {
                    if (_evaluator(exp))
                        found.Add(exp);
                    else
                        notEvaluatable = true;
                }
                notEvaluatable |= savedNotEvaluatable;
            }

            return exp;
        }
    }
}
