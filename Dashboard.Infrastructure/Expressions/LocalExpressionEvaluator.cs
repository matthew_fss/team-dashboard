﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Expressions
{
    public class LocalExpressionEvaluator : ExpressionVisitor
    {
        private HashSet<Expression> toEvaluate;
        private ExpressionFinder _finder;
        public LocalExpressionEvaluator() : this(null) { }

        public LocalExpressionEvaluator(params Func<Expression, bool>[] ignore)
        {
            _finder = new ExpressionFinder(exp =>
            {
                bool valid = exp.NodeType != ExpressionType.Parameter;

                int index = 0;
                while (valid && index < (ignore?.Length ?? 0))
                    valid = !(ignore[index++]?.Invoke(exp) ?? false);

                return valid;
            });
        }

        public Expression Evaluate(Expression exp)
        {
            toEvaluate = _finder.Find(exp);
            return Visit(exp);
        }

        public override Expression Visit(Expression exp)
        {
            if (exp == null)
                return null;

            if (toEvaluate.Contains(exp))
                return Compute(exp);

            return base.Visit(exp);
        }

        private Expression Compute(Expression exp)
        {
            if (exp.NodeType == ExpressionType.Constant)
                return exp;

            LambdaExpression lambda = Expression.Lambda(exp);
            Delegate fn = lambda.Compile();
            return Expression.Constant(fn.DynamicInvoke(null), exp.Type);
        }
    }
}
