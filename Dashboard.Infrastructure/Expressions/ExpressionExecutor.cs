﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Infrastructure.Expressions
{
    public static class ExpressionExecutor
    {
        public static object Execute(Expression exp)
        {
            return null;
        }

        public static T Execute<T>(Expression exp) => (T)Execute(exp);
    }
}
